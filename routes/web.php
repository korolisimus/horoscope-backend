<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'admin', 'middleware' => ['request', 'cors']], function () use ($router) {
    $router->group(['prefix' => 'astro'], function () use ($router) {
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AstroController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AstroController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AstroController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AstroController@delete'
        ]);
    });
    $router->group(['prefix' => 'daily'], function () use ($router) {
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\DailyController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\DailyController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\DailyController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\DailyController@delete'
        ]);
    });
    $router->group(['prefix' => 'zodiacs'], function () use ($router) {
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\ZodiacController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\ZodiacController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\ZodiacController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\ZodiacController@delete'
        ]);
    });
    $router->group(['prefix' => 'compare'], function () use ($router) {
        $router->get("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\CompareController@get'
        ]);
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\CompareController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\CompareController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\CompareController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\CompareController@delete'
        ]);
    });
    $router->group(['prefix' => 'haircut'], function () use ($router) {
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\HaircutController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\HaircutController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\HaircutController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\HaircutController@delete'
        ]);
    });
    $router->group(['prefix' => 'prediction'], function () use ($router) {
        $router->get("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\PredictionController@get'
        ]);
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\PredictionController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\PredictionController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\PredictionController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\PredictionController@delete'
        ]);
    });
    $router->group(['prefix' => 'fortune'], function () use ($router) {
        $router->post("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\FortuneController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\FortuneController@create'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\FortuneController@update'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\FortuneController@delete'
        ]);
    });
    $router->group(['prefix' => 'translate', 'middleware' => ['admin']], function () use ($router) {
        $router->get("/", [
            'uses' => 'Admin\TranslateController@list'
        ]);
        $router->get("/langs", [
            'uses' => 'Admin\TranslateController@languagesList'
        ]);
        $router->post("/key", [
            'uses' => 'Admin\TranslateController@storeKey'
        ]);
        $router->post("/lang", [
            'uses' => 'Admin\TranslateController@storeLang'
        ]);
        $router->post("/value", [
            'uses' => 'Admin\TranslateController@storeTranslate'
        ]);
        $router->delete("/key", [
            'uses' => 'Admin\TranslateController@deleteKey'
        ]);
        $router->delete("/lang", [
            'uses' => 'Admin\TranslateController@deleteLang'
        ]);
        $router->delete("/value", [
            'uses' => 'Admin\TranslateController@deleteTranslate'
        ]);
        $router->put("/key", [
            'uses' => 'Admin\TranslateController@updateKey'
        ]);
        $router->get("/export", [
            'uses' => 'Admin\TranslateController@export'
        ]);
        $router->post("/import", [
            'uses' => 'Admin\TranslateController@import'
        ]);
        $router->post('/send', [
            'uses' => 'Admin\TranslateController@send'
        ]);
        $router->post('/approve', [
            'uses' => 'Admin\TranslateController@approve'
        ]);
        $router->post('/publish', [
            'uses' => 'Admin\TranslateController@publish'
        ]);
        $router->post('/unpublished', [
            'uses' => 'Admin\TranslateController@unpublished'
        ]);
    });
    $router->group(['prefix' => 'admins'], function () use ($router) {
        $router->post("/login", [
            'uses' => 'Admin\AdminController@login'
        ]);
        $router->post("/logout", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@logout'
        ]);
        $router->put("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@update'
        ]);
        $router->get("/list", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@list'
        ]);
        $router->get("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@show'
        ]);
        $router->get("/get", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@get'
        ]);
        $router->post("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@register'
        ]);
        $router->delete("/", [
            'middleware' => ['admin'],
            'uses' => 'Admin\AdminController@delete'
        ]);
    });
    $router->group(['prefix' => 'notifications', 'middleware' => ['admin']], function () use ($router) {
        $router->get('/', 'Admin\PushNotificationsController@get');
        $router->post('/', 'Admin\PushNotificationsController@create');
        $router->put('/', 'Admin\PushNotificationsController@update');
        $router->delete('/', 'Admin\PushNotificationsController@delete');
        $router->post('/send', 'Admin\PushNotificationsController@send');
        $router->post('/copy', 'Admin\PushNotificationsController@copy');
    });
    $router->get('/languages', ['middleware' => 'admin', 'uses' => 'Controller@languages']);
    $router->get('/countries', ['middleware' => 'admin', 'uses' => 'Controller@countries']);
});
$router->group(['prefix' => 'api/v1', 'middleware' => ['request', 'cors']], function ($router) {
    $router->post('/install', 'App\UserController@install');
    $router->group(['prefix' => 'users'], function ($router) {
        $router->get('/', 'App\UserController@getUserOrCreate');
        $router->post('/', 'App\UserController@getUserOrCreate');
        $router->post('/social', 'App\UserController@socialRegistration');
        $router->post('/install', 'App\UserController@install');

        $router->group(['middleware' => ['user']], function ($router) {
            $router->post('/subscribe', 'App\UserController@subscribe');
            $router->post('/subscribe_fail', 'App\UserController@subscribeFail');
            $router->post('/is_subscribed', 'App\UserController@isSub');
            $router->put('/', 'App\UserController@update');
        });
    });
    $router->group(['middleware' => ['user']], function ($router) {
        $router->get('/updateMonth', 'Controller@updateMonth');
        $router->get('/updateDaily', 'Controller@updateDaily');
        $router->get('/push', 'Controller@pushDev');
        $router->get('/haircut', 'App\HaircutController@get');
        $router->get('/compare', 'App\CompareController@get');
        $router->get('/daily', 'App\DailyController@get');
        $router->group(['prefix' => 'coffee'], function ($router) {
            $router->get('/', 'App\FortuneCoffeeController@get');
            $router->post('/', 'App\FortuneCoffeeController@storeFiles');
            $router->delete('/', 'App\FortuneCoffeeController@delete');
            $router->post('/refresh', 'App\FortuneCoffeeController@refresh');
        });
        $router->group(['prefix' => 'hand'], function ($router) {
            $router->get('/', 'App\FortuneHandController@get');
            $router->post('/', 'App\FortuneHandController@storeFiles');
            $router->delete('/', 'App\FortuneHandController@delete');
            $router->post('/refresh', 'App\FortuneHandController@refresh');
        });
        $router->get('/astro', 'App\AstroController@get');
        $router->get('/zodiac', 'App\ZodiacController@get');
    });
    $router->get('/translates', ['middleware' => ['user'], 'uses' => 'Controller@translates']);
});
$router->post('/bill/callback', 'App\UserController@billCallback');

