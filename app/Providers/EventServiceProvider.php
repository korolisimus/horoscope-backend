<?php declare(strict_types=1);

namespace App\Providers;


use App\Events\Admin\CreateAdminModelEvent;
use App\Events\Admin\DeleteAdminModelEvent;
use App\Events\Admin\UpdateAdminModelEvent;
use App\Events\Admin\UpdateKeyValueEvent;
use App\Listeners\Admin\CreateAdminModelListener;
use App\Listeners\Admin\DeleteAdminModelListener;
use App\Listeners\Admin\UpdateAdminModelListener;
use App\Listeners\Admin\UpdateKeyValueListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * @var \string[][]
     */
    protected $listen = [
        UpdateKeyValueEvent::class => [
            UpdateKeyValueListener::class
        ],
        CreateAdminModelEvent::class => [
            CreateAdminModelListener::class,
            UpdateAdminModelListener::class
        ],
        UpdateAdminModelEvent::class => [
            UpdateAdminModelListener::class
        ],
        DeleteAdminModelEvent::class => [
            DeleteAdminModelListener::class
        ]
    ];
}
