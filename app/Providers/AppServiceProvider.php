<?php

namespace App\Providers;

use App\Manager;
use App\Services\AuthService;
use App\Services\UserAuthService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('auth', function ($app) {
            return app(AuthService::class);
        });

        $this->app->singleton('manager', function ($app) {
            return app(Manager::class);
        });

        $this->app->singleton('user', function ($app) {
            return app(UserAuthService::class);
        });
    }
}
