<?php

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * Class Translate
 * @package App\Validators\Repositories
 */
class TranslateValidator extends LaravelValidator
{
    const RULE_DELETE = 'delete';
    const RULE_CREATE_TRANSLATE = 'create_translate';
    const RULE_CREATE_KEY = 'create_key';
    const RULE_CREATE_LANG = 'create_lang';

    const RULE_UPDATE_KEY = 'update_key';
    const RULE_UPDATE_LANG = 'update_lang';
    const RULE_UPDATE_LANG_STATUS = 'update_lang_status';

    const RULE_DELETE_TRANSLATE = 'delete_translate';
    const RULE_DELETE_LANG = 'delete_lang';
    const RULE_DELETE_KEY = 'delete_key';

    const RULE_APP_TRANSLATE = 'app_translate';


    protected $rules = [
        self::RULE_CREATE_TRANSLATE => [
            'id' => 'required',
            'langs' => 'required'
        ],
        self::RULE_CREATE_KEY => [
            'key' => 'required'
        ],
        self::RULE_CREATE_LANG => [
            'lang' => 'required',
            'status' => 'in:1,0'
        ],
        self::RULE_DELETE_TRANSLATE => [
            'id' => 'required',
            'lang' => 'required',
        ],
        self::RULE_DELETE_KEY => [
            'id' => 'required'
        ],
        self::RULE_DELETE_LANG => [
            'lang' => 'required'
        ],
        self::RULE_UPDATE_KEY => [
            'id' => 'required',
            'key' => 'required'
        ],
        self::RULE_UPDATE_LANG => [
            'id' => 'required',
            'lang' => 'required'
        ],
        self::RULE_UPDATE_LANG_STATUS => [
            'lang' => 'required',
        ],
        self::RULE_APP_TRANSLATE => [
            'lang' => 'required'
        ]
    ];
}