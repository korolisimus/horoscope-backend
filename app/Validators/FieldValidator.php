<?php declare(strict_types=1);

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class FieldValidator extends LaravelValidator
{
    public const RULE_ID = 'id';
    public const RULE_TYPE = 'type';
    public const RULE_TEMPLATE_ID = 'template_id';
    public const RULE_STATUS = 'status';

    protected $rules = [
        self::RULE_ID => [
            'id' => 'required'
        ],
        self::RULE_TYPE => [
            'type' => 'required'
        ],
        self::RULE_TEMPLATE_ID => [
            'template_id' => 'required'
        ],
        self::RULE_STATUS => [
            'status' => 'required'
        ]
    ];
}