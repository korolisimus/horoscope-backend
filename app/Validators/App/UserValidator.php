<?php declare(strict_types=1);

namespace App\Validators\App;

use Prettus\Validator\LaravelValidator;

/**
 * Class User
 * @package App\Validators
 */
class UserValidator extends LaravelValidator
{
    const RULE_REGISTRATION = 'rule_registration';

    protected $rules = [
        self::RULE_REGISTRATION => [
            'platform' => 'required',
            'bundle_id' => 'required',
            'lang' => 'required'
        ]
    ];
}
