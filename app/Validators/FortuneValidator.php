<?php declare(strict_types=1);

namespace App\Validators;

use Prettus\Validator\LaravelValidator;

class FortuneValidator extends LaravelValidator
{
    public const RULE_API = 'api';

    protected $rules = [
        self::RULE_API => [
            'type' => 'required|in:hand,coffee'
        ],
    ];
}