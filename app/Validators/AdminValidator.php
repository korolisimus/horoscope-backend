<?php declare(strict_types=1);

namespace App\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

/**
 * Class Admin
 * @package App\Validators
 */
class AdminValidator extends LaravelValidator
{
    public const RULE_LOGIN = 'login';
    public const RULE_REGISTER = 'register';
    public const RULE_DELETE = 'delete';
    public const RULE_GET = 'get';

    protected $rules = [
        ValidatorInterface::RULE_UPDATE => [
            'id' => 'required',
            'role_id' => 'in:1,2',
        ],
        self::RULE_LOGIN => [
            'email' => 'required',
            'password' => 'required'
        ],
        self::RULE_REGISTER => [
            'role_id' => 'required|in:1,2',
            'email' => 'required',
            'password' => 'required'
        ],
        self::RULE_DELETE => [
            'id' => 'required'
        ],
        self::RULE_GET => [
            'id' => 'required'
        ]
    ];

    protected $messages = [
        'id.email' => 'EMAIL_REQUIRED',
        'id.password' => 'PASSWORD_REQUIRED'
    ];
}
