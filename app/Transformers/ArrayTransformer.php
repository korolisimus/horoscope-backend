<?php declare(strict_types=1);

namespace App\Transformers;

use App\Models\ModelAbstract;
use League\Fractal\TransformerAbstract;

class ArrayTransformer extends TransformerAbstract
{
    public function transform(ModelAbstract $model): array
    {
        return $model->toArray();
    }
}