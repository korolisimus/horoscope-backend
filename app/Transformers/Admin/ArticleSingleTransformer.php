<?php declare(strict_types=1);

namespace App\Transformers\Admin;

use App\Models\ModelAbstract;
use League\Fractal\TransformerAbstract;

class ArticleSingleTransformer extends TransformerAbstract
{
    public function transform(ModelAbstract $model): array
    {
        $data = $model->toArray();
        $data['active'] = $data['published_entity']['status'] ?? null;
        return $data;
    }
}