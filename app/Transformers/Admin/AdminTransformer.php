<?php declare(strict_types=1);

namespace App\Transformers\Admin;

use App\Models\Admin;
use League\Fractal\TransformerAbstract;

/**
 * Class AdminTransformer
 * @package App\Transformers\Admin
 */
class AdminTransformer extends TransformerAbstract
{
    /**
     * @param \App\Models\Admin $model
     * @return array
     */
    public function transform(Admin $model): array
    {
        $data = $model->toArray();
        $data['token'] = app('auth')->auth()->token;
        return $data;
    }
}