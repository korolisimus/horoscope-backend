<?php declare(strict_types=1);

namespace App\Transformers\Admin;

use League\Fractal\TransformerAbstract;

class TranslateKeysListTransformer extends TransformerAbstract
{
    public function transform($model): array
    {
        $data = $model->toArray();
        $needValues = array("value", "is_valid");
        $data['langs'] = array_combine(
            array_column($data['langs'], 'lang'),
            array_map(function ($item) use ($needValues) {
                return array_intersect_key($item, array_flip($needValues));
            }, $data['langs']));
        return $data;
    }
}