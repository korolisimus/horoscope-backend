<?php declare(strict_types=1);

namespace App\Transformers\App;

use App\Models\ModelAbstract;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(ModelAbstract $model): array
    {
        $data = $model->toArray();
        $data['token'] = app('auth')->auth()->token;
        return $data;
    }
}