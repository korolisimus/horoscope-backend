<?php declare(strict_types=1);

namespace App\Services\App;

use App\Actions\AccessToken\CreateAccessTokenAction;
use App\Actions\Api\Adcbill\InstallAction;
use App\Actions\User\IsSubscribeAction;
use App\Actions\User\UserLogoutAction;
use App\Actions\User\UserRegistrationAction;
use App\Actions\User\UserSubscribeAction;
use App\Constants\ArticleTypesConstant;
use App\Criterias\Core\EqualCriteria;
use App\Models\User;
use App\Tasks\GetByIdTask;
use App\Tasks\GetFirstTask;
use App\Tasks\UpdateTask;
use App\Traits\ManagerTrait;
use App\Transformers\App\UserTransformer;


class UserService
{
    use ManagerTrait;

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function getUserOrCreate(array $params = []): ?array
    {
        try {
            $access = app('auth')->auth($params);
        } catch (\Throwable $e) {
            $user = $this->manager->action(UserRegistrationAction::class)
                ->arguments(['params' => $params])
                ->run();
            $access = $this->manager->action(CreateAccessTokenAction::class)
                ->arguments(['id' => $user->id, 'type' => 'user'])
                ->run();
        }
        app('auth')->auth(['token' => $access->token]);
        return $this->get();
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function get(array $params = []): array
    {
        $userId = app('auth')->auth($params)->entity;
        return $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $userId])
            ->model(\App\Models\User::class)
            ->transformer(UserTransformer::class)
            ->run();
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function update(array $params = []): array
    {
        $user_id = app('auth')->auth($params)->entity;
        $this->manager->task(UpdateTask::class)
            ->arguments(['id' => $user_id, 'params' => $params])
            ->model(\App\Models\User::class)
            ->run();

        $coffeeStatus = $params['coffee_status'] ?? null;
        $user = app('user')->auth();
        if ($coffeeStatus == 'process') {
            $tmp = $user->prediction_settings;
            $tmp[ArticleTypesConstant::FORTUNE_COFFEE] = null;
            $user->prediction_settings = $tmp;
            $user->coffee_process_date = date('Y-m-d H:i:s', time());
            $user->save();
        }
        
        $handStatus = $params['hand_status'] ?? null;
        if ($handStatus == 'process') {
            $tmp = $user->prediction_settings;
            $types = ArticleTypesConstant::FORTUNE_HAND;
            foreach ($types as $type) {
                $tmp[$type] = null;
            }
            $user->prediction_settings = $tmp;
            $user->hand_process_date = date('Y-m-d H:i:s', time());
            $user->save();
        }

        return $this->get();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function logout(array $params = []): array
    {
        return $this->manager->action(UserLogoutAction::class)
            ->arguments(['params' => $params])->run();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function isSub(array $params = []): array
    {
        $this->manager->action(IsSubscribeAction::class)
            ->arguments([$params])->run();
        return $this->get();
    }

    /**
     * @param array $params
     * @return array|mixed|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function subscribe(array $params = []): array
    {
        $this->manager->action(UserSubscribeAction::class)->arguments([$params])->run();
        $user = app('user')->auth($params);
        $sub = $params['sub'] ?? false;
        if (!empty($sub)) {
            $user->subscribed = 'sub';
            $user->save();
        }
        return $this->get();
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function install(array $params = []): array
    {
        $this->manager->action(InstallAction::class)->arguments([$params])->run();
        return ['data' => true];
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function billCallback(array $params = []): array
    {
        $type = $params['type'] ?? null;
        if ($type == 'update_subs') {
            $uuid = $params['data']['uuid'] ?? null;
            $user = $this->manager->task(GetFirstTask::class)
                ->criteria(new EqualCriteria('uuid', $uuid))
                ->model(User::class)->run();
            if (!empty($user)) {
                $status = $params['data']['status'] ?? null;
                if (empty($status)) {
                    $user->subscribed = 'no-sub';
                } else {
                    $user->subscribed = 'sub';
                }
                $user->save();
            }
        }
        return ['data' => true];
    }

    /**
     * @param array $params
     * @return array|mixed|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function processHandPrediction(array $params = [])
    {
        $user = app('user')->auth($params);
        $user->hand_status = 'process';
        $user->save();

        return $this->get();
    }

    /**
     * @return array|mixed|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function processCoffeePrediction(array $params = [])
    {
        $user = app('user')->auht($params);
        $user->coffee_status = 'process';
        $user->save();

        return $this->get();
    }
}
