<?php declare(strict_types=1);

namespace App\Services\App;

use App\Constants\ArticleTypesConstant;

class ZodiacService extends ArticleService
{
    public function get(array $params = []): array
    {
        $result = [];
        $key = $params['key'] ?? null;
        foreach (['daily', 'month'] as $type){
            $params['type'] = ArticleTypesConstant::ZODIAC . '_' . $type;
            $data = parent::get($params);
            $tmp = [];
            if(!empty($key)){
                foreach ($data['options'] as $item) {
                    $tmp[$item['key']] = $item['value'];
                }
            }else{
                foreach ($data['data'] as $i) {
                    foreach ($i['options'] as $item) {
                        $tmp[$i['key']][$item['key']] = $item['value'];
                    }
                }
            }
            $result[$type] = $tmp;
        }
        return ['data' => $result];
    }
}