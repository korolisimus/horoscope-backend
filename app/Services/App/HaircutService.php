<?php declare(strict_types=1);

namespace App\Services\App;

use App\Constants\ArticleTypesConstant;

class HaircutService extends ArticleService
{
    public function get(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::HAIRCUT;
        $data = parent::get($params);
        $result = [];
        $key = 1;
        foreach ($data['data'] as $item) {
            $result[$key++] = empty($item['value']) ? null : $item['value'];
        }
        return ['data' => $result];
    }
}