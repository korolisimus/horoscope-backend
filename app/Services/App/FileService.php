<?php declare(strict_types=1);

namespace App\Services\App;

use App\Actions\Files\DeleteFilesAction;
use App\Actions\Files\StoreBase64Action;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Models\File;
use App\Tasks\GetTask;
use App\Tasks\MultipleUpdateTask;
use App\Traits\ManagerTrait;
use App\Validators\FieldValidator;

class FileService
{
    use ManagerTrait;

    /**
     * @param array $params
     * @return bool[]
     * @throws \Exception
     */
    public function delete(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $userId = app('auth')->auth()->entity;
        $files = $this->manager->task(GetTask::class)
            ->criteria(new EqualCriteria('id', $params['id']))
            ->criteria(new EqualCriteria('user_id', $userId))
            ->model(File::class)
            ->run();
        $this->manager->action(DeleteFilesAction::class)
            ->arguments(['files' => $files])->run();
        return ['data' => true];
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function storeFiles(array $params = [])
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_TEMPLATE_ID);
        $type = $params['type'];
        $ids = $this->manager->action(StoreBase64Action::class)
            ->arguments([$params])->run();
        $access = app('auth')->auth();
        return $this->manager->task(MultipleUpdateTask::class)
            ->criteria(new InCriteria('id', $ids))
            ->model(File::class)
            ->arguments([['type' => $type, 'user_id' => $access->entity, 'template_id' => $params['template_id']]])
            ->run();
    }
}
