<?php declare(strict_types=1);

namespace App\Services\App;

use App\Actions\Files\DeleteFilesAction;
use App\Actions\Fortune\GetUserCoffeeFilesAction;
use App\Actions\Fortune\GetUserCoffeePredictionStatusAction;
use App\Constants\ArticleTypesConstant;

/**
 * Class FortuneCoffeeService
 * @package App\Services\App
 */
class FortuneCoffeeService extends ArticleService
{
    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function storeFiles(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::FORTUNE_COFFEE;
        /** @var \App\Services\App\FileService $fileService */
        $fileService = app(FileService::class);
        $fileService->storeFiles($params);
        return $this->get();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function get(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::FORTUNE_COFFEE;
        $user = app('user')->auth();
        $params['id']  = $user->prediction_settings[ArticleTypesConstant::FORTUNE_COFFEE] ?? null;
        $data = parent::get($params);
        $data['prediction_status'] = $this->manager->action(GetUserCoffeePredictionStatusAction::class)->run();
        $user = app('user')->auth($params);
        $coffeeFiles = $user->coffeeFiles()->get();
        $files = array_fill(0, 3, null);
        foreach ($coffeeFiles as $file) {
            $files[$file->template_id] = $file;
        }
        $data['files'] = $files;
        return ['data' => $data];

    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function refresh(array $params = []): array
    {
        $files = $this->manager->action(GetUserCoffeeFilesAction::class)->arguments(['params' => $params])->run();
        $this->manager->action(DeleteFilesAction::class)
            ->arguments(['files' => $files])->run();
        return $this->get();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function delete(array $params): array
    {
        /** @var \App\Services\App\FileService $fileService */
        $fileService = app(FileService::class);
        $fileService->delete($params);
        return $this->get();
    }
}
