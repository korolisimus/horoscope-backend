<?php declare(strict_types=1);

namespace App\Services\App;

use App\Actions\Api\Google\DetectImageAction;
use App\Actions\Files\DeleteFilesAction;
use App\Actions\Files\MakeBase64DataIteratorAction;
use App\Actions\Fortune\DeleteUserFilesByTypeAction;
use App\Actions\Fortune\GetUserHandFilesAction;
use App\Actions\Fortune\GetUserHandPredictionStatusAction;
use App\Actions\Fortune\IsHandAction;
use App\Constants\ArticleTypesConstant;


class FortuneHandService extends ArticleService
{
    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function storeFiles(array $params = []): array
    {
        $params['type'] = 'hand';
        $params['template_id'] = 1;

        $iterator = $this->manager->action(MakeBase64DataIteratorAction::class)
            ->arguments([$params['file']])->run();

        $item = $iterator->current();

        if (empty($item)) {
            return $this->get();
        }

        $base = explode(',', $item)[1];
        $data = $this->manager->action(DetectImageAction::class)
            ->arguments([$base])->run();
        $isHand = $this->manager->action(IsHandAction::class)
            ->arguments([$data])->run();


        if (!$isHand) {
            throw new \Exception('IMAGE_NOT_VALID', 423);
        }

        $userId = app('auth')->auth()->entity;
        $this->manager->action(DeleteUserFilesByTypeAction::class)->arguments([$userId, 'hand'])->run();
        /** @var \App\Services\App\FileService $fileService */
        $fileService = app(FileService::class);
        $fileService->storeFiles($params);

        return $this->get();
    }

    public function get(array $params = []): array
    {
        $types = ArticleTypesConstant::FORTUNE_HAND;
        $result = [];

        $user = app('user')->auth();
        $predictionSettings = $user->prediction_settings;

        foreach ($types as $type) {
            $params['type'] = $type;
            $params['id'] = $predictionSettings[$type] ?? null;
            $info = parent::get($params);
            $result[$type] = $info['text'];
        }
        $result['prediction_status'] = $this->manager->action(GetUserHandPredictionStatusAction::class)->run();
        $user = app('user')->auth($params);
        $handFiles = $user->handFiles()->get();
        $result['file'] = null;
        if ($handFiles->count()) {
            $result['file'] = $handFiles->first();
        }
        return ['data' => $result];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function refresh(array $params = []): array
    {
        $files = $this->manager->action(GetUserHandFilesAction::class)->arguments(['params' => $params])->run();
        $this->manager->action(DeleteFilesAction::class)
            ->arguments(['files' => $files])->run();
        return $this->get();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function delete(array $params): array
    {
        /** @var \App\Services\App\FileService $fileService */
        $fileService = app(FileService::class);
        $fileService->delete($params);
        $userId = app('auth')->auth()->entity;
        $this->manager->action(DeleteUserFilesByTypeAction::class)->arguments([$userId, 'hand'])->run();
        return $this->get();
    }
}