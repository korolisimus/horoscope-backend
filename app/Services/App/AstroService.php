<?php declare(strict_types=1);

namespace App\Services\App;

use App\Constants\ArticleTypesConstant;

class AstroService extends ArticleService
{
    public function get(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::ASTRO;
        $data = parent::get($params);
        return ['data' => $data];
    }
}