<?php declare(strict_types=1);

namespace App\Services\App;


use App\Constants\ArticleTypesConstant;

class DailyService extends ArticleService
{
    public function get(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::DAILY;
        $key = $params['key'] ?? null;
        $data = parent::get($params);
        $result = [];
        if (!empty($key)) {
            foreach ($data['options'] as $item) {
                $result[$item['key']] = $item['value'];
            }
        } else {
            foreach ($data['data'] as $i) {
                foreach ($i['options'] as $item) {
                    $result[$i['key']][$item['key']] = $item['value'];
                }
            }
        }

        return ['data' => $result, 'info' => $data['info'] ?? null];
    }
}
