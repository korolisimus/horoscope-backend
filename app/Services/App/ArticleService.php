<?php declare(strict_types=1);

namespace App\Services\App;

use App\Actions\Article\MakeNullObjectAction;
use App\Actions\Translates\TranslatePublishEntityAction;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\OrderByRawCriteria;
use App\Models\PublishedEntity;
use App\Tasks\GetFirstTask;
use App\Traits\ManagerTrait;

class ArticleService
{
    use ManagerTrait;
    
    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function get(array $params = []): array
    {
        $type = $params['type'] ?? null;
        $key = $params['key'] ?? null;
        $id = $params['id'] ?? null;
        $entity = null;
        if (in_array($type, ['coffee', 'mind', 'heart', 'destiny', 'life']) && !empty($id)) {
            $entity = $this->manager->task(GetFirstTask::class)
                ->criteria(new EqualCriteria('id', $id))
                ->model(PublishedEntity::class)
                ->arguments([$id])->run();
        }

        if (empty($entity)) {
            $this->manager->task(GetFirstTask::class)->model(PublishedEntity::class);
            if (in_array($type, ['coffee', 'mind', 'heart', 'destiny', 'life'])) {
                $this->manager->criteria(new OrderByRawCriteria('rand()'));
            } else {
                $this->manager->criteria(new EqualCriteria('status', '1'));
            }
            $this->manager->criteria(new EqualCriteria('entity_type', $type));

            $entity = $this->manager->run();
            $id = $entity->id ?? null;

            if (in_array($type, ['coffee', 'mind', 'heart', 'destiny', 'life'])) {
                $user = app('user')->auth();
                $tmp = $user->prediction_settings;
                $tmp[$type] = $id;
                $user->prediction_settings = $tmp;
                $user->save();
            }
        }

        if (empty($entity)) {
            $entity = new PublishedEntity();
            $entity->data = $this->manager->action(MakeNullObjectAction::class)
                ->arguments(['type' => $type])->run();
        }

        $data = $entity->data;
        if (!empty($key)) {
            $index = array_search($key, array_column($data['data'], 'key'));
            if ($index !== false) {
                $data = $entity->data['data'][$index];
                $data['info'] = $entity->data['info'] ?? null;
            }
        }

        $lang = $params['lang'] ?? 'ru';

        return $this->manager->action(TranslatePublishEntityAction::class)
            ->arguments(['data' => $data, 'lang' => $lang])->run();
    }
}
