<?php declare(strict_types=1);

namespace App\Services\App;

use App\Constants\ArticleTypesConstant;

class CompareService extends ArticleService
{
    public function get(array $params = []): array
    {
        $params['type'] = ArticleTypesConstant::COMPARE;
        $data = parent::get($params);
        $result = [];

        $key = $params['key'] ?? null;
        if (!empty($key)) {
            foreach ($data['options'] as $item) {
                $result[$item['key']] = $item['value'];
            }
        } else {
            foreach ($data['data'] as $i) {
                foreach ($i['options'] as $item) {
                    $result[$i['key']][$item['key']] = $item['value'];
                }
            }
        }

        return ['data' => $result];
    }
}