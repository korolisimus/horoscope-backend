<?php declare(strict_types=1);

namespace App\Services;

use App\Actions\User\GetAuthUserAction;
use App\Models\User;

class UserAuthService
{
    private User $user;

    public function auth(array $params = [], $force = false): User
    {
        if (empty($this->user) || $force) {
            $this->user = app('manager')->action(GetAuthUserAction::class)->arguments([$params])->run();
        }
        return $this->user;
    }
}
