<?php declare(strict_types=1);

namespace App\Services;

use App\Models\AccessToken;
use App\Tasks\GetByFieldTask;
use Illuminate\Http\Request;


class AuthService
{
    private AccessToken $access;

    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param array $params
     * @param $force
     * @return \App\Models\AccessToken|mixed|null
     * @throws \Exception
     */
    public function auth(array $params = [], $force = false): ?AccessToken
    {

        if (!empty($this->access) && !$force) {
            return $this->access;
        }
        $this->request->merge($params);
        $params = $this->request->all();

        $token = $params['token'] ?? null;
        if (empty($token)) {
            throw new \Exception('NOT_AUTHORIZED', 401);
        }

        $manager = app('manager');
        $access = $manager->task(GetByFieldTask::class)
            ->arguments(['field' => 'token', 'value' => $token])
            ->model(AccessToken::class)->run();

        if (empty($access)) {
            throw new \Exception('TOKEN_NOT_VALID', 401);
        }

        $this->access = $access;

        return $this->access;
    }
}