<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Haircut;

class HaircutService extends ServiceAbstract
{
    protected string $model = Haircut::class;
}