<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Fortune;

class FortuneService extends ArticleService
{
    protected string $model = Fortune::class;
}
