<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Daily;


class DailyService extends ServiceAbstract
{
    protected string $model = Daily::class;
}