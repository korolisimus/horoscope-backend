<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Zodiac;

/**
 * Class ZodiacService
 * @package App\Services\Admin
 */
class ZodiacService extends ServiceAbstract
{
    protected string $model = Zodiac::class;
}