<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Criterias\Core\OrderByCriteria;
use App\Criterias\RequestCriteria;
use App\Events\Admin\CreateAdminModelEvent;
use App\Events\Admin\DeleteAdminModelEvent;
use App\Events\Admin\UpdateAdminModelEvent;
use App\Tasks\CreateTask;
use App\Tasks\DeleteByIdTask;
use App\Tasks\GetByIdTask;
use App\Tasks\GetTask;
use App\Tasks\PaginateTask;
use App\Tasks\UpdateTask;
use App\Traits\ManagerTrait;
use App\Transformers\Admin\ArticleListTransformer;
use App\Transformers\Admin\ArticleSingleTransformer;
use App\Validators\FieldValidator;

/**
 * Class ServiceAbstract
 * @package App\Services
 */
abstract class ServiceAbstract
{
    use ManagerTrait;

    protected string $model;
    protected string $listTransformer = ArticleListTransformer::class;
    protected string $singleTransformer = ArticleSingleTransformer::class;

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function create(array $params = []): array
    {
        $item = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model($this->model)
            ->run();
        event(new CreateAdminModelEvent($item->id));
        return $this->get(['id' => $item->id]);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function get(array $params = []): array
    {
        $id = $params['id'] ?? null;
        $id = (int)$id;
        if (empty($id)) {
            $limit = $params['limit'] ?? null;
            if (!is_null($limit)) {
                $result = $this->manager->task(PaginateTask::class)
                    ->arguments(['limit' => (int)$limit])
                    ->criteria(new RequestCriteria($params))
                    ->criteria(new OrderByCriteria('id', 'desc'))
                    ->method('with',['publishedEntity'])
                    ->model($this->model)
                    ->transformer($this->listTransformer)
                    ->run();
            } else {
                $result = $this->manager->task(GetTask::class)
                    ->model($this->model)
                    ->criteria(new RequestCriteria($params))
                    ->criteria(new OrderByCriteria('id', 'desc'))
                    ->method('with',['publishedEntity'])
                    ->transformer($this->listTransformer)
                    ->run();
            }
        } else {
            $result = $this->manager->task(GetByIdTask::class)
                ->arguments(['id' => $id])
                ->model($this->model)
                ->method('with',['publishedEntity'])
                ->transformer($this->singleTransformer)
                ->run();
        }
        return $result;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function update(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $id = (int)$params['id'];
        $this->manager->task(UpdateTask::class)
            ->arguments(['id' => $id, 'params' => $params])
            ->model($this->model)
            ->run();
        event(new UpdateAdminModelEvent($id));
        return $this->get(['id' => $id]);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function delete(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $id = (int)$params['id'];
        event(new DeleteAdminModelEvent($id));
        $result = $this->manager->task(DeleteByIdTask::class)
            ->arguments(['id' => $id])
            ->model($this->model)
            ->run();
        return ['data' => $result];
    }
}
