<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Actions\AccessToken\DeleteRegisteredAccessAction;
use App\Actions\AccessToken\RegistrationAccessAction;
use App\Actions\Admin\CheckAdminEmailExistAction;
use App\Actions\Admin\CreateAdminAccessTokenAction;
use App\Actions\Admin\GetAdminByEmailPasswordAction;
use App\Actions\Admin\GetAdminByIdAction;
use App\Actions\Admin\UpdateAdminByIdAction;
use App\Models\Admin;
use App\Tasks\CreateTask;
use App\Tasks\DeleteByIdTask;
use App\Tasks\PaginateTask;
use App\Transformers\Admin\AdminListTransformer;
use App\Validators\AdminValidator;
use App\Validators\FieldValidator;


class AdminService extends ServiceAbstract
{
    /**
     * @return array
     * @throws \Exception
     */
    public function show(): array
    {
        $access = app('auth')->auth();
        return $this->get(['id' => $access->entity]);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function login(array $params = []): array
    {
        $admin = $this->manager->action(GetAdminByEmailPasswordAction::class)->arguments([$params])->run();
        $access = $this->manager->action(CreateAdminAccessTokenAction::class)->arguments([$admin])->run();
        $this->manager->action(RegistrationAccessAction::class)->arguments([$access])->run();
        return $this->show();
    }

    public function get(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        return $this->manager->action(GetAdminByIdAction::class)->arguments([(int)$params['id']])->run();
    }

    /**
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function logout(): array
    {
        $result = $this->manager->action(DeleteRegisteredAccessAction::class)->run();
        return ['data' => $result];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function register(array $params = []): array
    {
        app(AdminValidator::class)->with($params)->passesOrFail(AdminValidator::RULE_REGISTER);
        $admin = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model(Admin::class)
            ->run();
        return $this->get(['id' => $admin->id]);
    }

    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function delete(array $params = []): array
    {
        app(AdminValidator::class)->with($params)->passesOrFail(AdminValidator::RULE_DELETE);
        $data = $this->manager->task(DeleteByIdTask::class)
            ->arguments(['id' => $params['id']])
            ->model(Admin::class)
            ->run();
        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function list(array $params = []): array
    {
        $limit = $params['limit'] ?? 10;
        return $this->manager->task(PaginateTask::class)
            ->arguments(['limit' => $limit])
            ->model(Admin::class)
            ->transformer(AdminListTransformer::class)
            ->run();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function update(array $params = []): array
    {
        app(AdminValidator::class)->with($params)->passesOrFail(AdminValidator::RULE_UPDATE);
        $this->manager->action(CheckAdminEmailExistAction::class)->arguments([$params])->run();
        $this->manager->action(UpdateAdminByIdAction::class)->arguments([$params['id'], $params])->run();
        return $this->get(['id' => $params['id']]);
    }
}
