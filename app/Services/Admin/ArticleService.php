<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Article;
use App\Transformers\Admin\ArticleListTransformer;
use App\Transformers\Admin\ArticleSingleTransformer;

/**
 * Class ArticleService
 * @package App\Services\Admin
 */
class ArticleService extends ServiceAbstract
{
    protected string $model = Article::class;
    protected string $listTransformer = ArticleListTransformer::class;
    protected string $singleTransformer = ArticleSingleTransformer::class;
}
