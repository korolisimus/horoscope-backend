<?php declare(strict_types=1);

namespace App\Services\Admin;


use App\Actions\Notifications\PushAction;
use App\Criterias\Core\DateCriteria;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Criterias\Core\NotEqualCriteria;
use App\Criterias\Core\NotNullCriteria;
use App\Criterias\Core\OrderByCriteria;
use App\Criterias\PushNotifications\SearchCriteria;
use App\Models\PushNotification;
use App\Models\User;
use App\Tasks\CreateTask;
use App\Tasks\DeleteByIdTask;
use App\Tasks\GetByIdTask;
use App\Tasks\GetTask;
use App\Tasks\PaginateTask;
use App\Tasks\UpdateTask;
use App\Traits\ManagerTrait;
use App\Validators\FieldValidator;
use Carbon\Carbon;

/**
 * Class PushNotifications
 * @package App\Services
 */
class PushNotificationsService
{
    use ManagerTrait;

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function create(array $params = [])
    {
        $item = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model(PushNotification::class)->run();
        return $this->get(['id' => $item->id]);
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function get(array $params = [])
    {
        $id = $params['id'] ?? null;
        if (!empty($id)) {
            $data = $this->manager->task(GetByIdTask::class)
                ->arguments(['id' => (int)$id])
                ->model(PushNotification::class)->run();
        } else {
            $limit = $params['limit'] ?? 10;
            $data = $this->manager->task(PaginateTask::class)
                ->arguments(['limit' => $limit])
                ->model(PushNotification::class)
                ->criteria(new SearchCriteria($params))
                ->criteria(new OrderByCriteria('id', 'desc'))
                ->run();
        }
        return $data;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function delete(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $data = $this->manager->task(DeleteByIdTask::class)
            ->arguments(['id' => (int)$params['id']])
            ->model(PushNotification::class)->run();
        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function update(array $params = [])
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $item = $this->manager->task(UpdateTask::class)
            ->arguments(['id' => (int)$params['id'], 'params' => $params])
            ->model(PushNotification::class)->run();
        return $this->get(['id' => $item->id]);
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function copy(array $params = [])
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $item = $this->manager->task(GetByIdTask::class)->arguments(['id' => $params['id']])
            ->model(PushNotification::class)->run();
        $new_item = $item->replicate();
        $new_item->status = 0;
        $new_item->success = 0;
        $new_item->failure = 0;
        $new_item->save();
        return $this->get(['id' => $new_item->id]);
    }

    /**
     * @throws \Exception
     */
    public function autoSend()
    {
        $notifications = $this->manager->task(GetTask::class)
            ->model(PushNotification::class)
            ->criteria(new EqualCriteria('status', 0))
            ->criteria(new DateCriteria('date', Carbon::now(), '<'))->run();
        foreach ($notifications as $notification) {
            $this->send($notification->id);
        }
    }

    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function send(array $params = [])
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);


        $notification = $this->manager->task(GetByIdTask::class)->arguments(['id' => $params['id']])
            ->model(PushNotification::class)->run();

        $this->manager->task(GetTask::class)
            ->model(User::class)
            ->criteria(new EqualCriteria('bundle_id', 'com.adcombo.horoscope'))
            ->criteria(new NotNullCriteria('fcm'))
            ->criteria(new NotEqualCriteria('fcm', ''));


        if (!empty($notification->geos)) {
            $this->manager->criteria(new InCriteria('country', $notification->geos));
        }
        if (!empty($notification->platforms)) {
            $this->manager->criteria(new InCriteria('platform', $notification->platforms));
        }

        $users = $this->manager->run()->toArray();

        $ids = array_column($users, 'fcm');

        $res = $this->manager->action(PushAction::class)
            ->arguments(['params' => [
                'to' => $ids,
                'title' => $notification->name,
                'body' => $notification->text
                //'extra' => ['match' => $notification->match]
            ]]);

        $notification->success = $res['success'] ?? 0;
        $notification->failure = $res['failure'] ?? 0;
        $notification->status = 1;
        $notification->save();
        return $this->get(['id' => $notification->id]);
    }
}
