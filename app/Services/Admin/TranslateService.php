<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Actions\Article\GetBindKeyValuesAction;
use App\Actions\Article\MakeModelByIdAction;
use App\Actions\Article\PublishArticleAction;
use App\Actions\Article\SendToTranslateArticleAction;
use App\Actions\Article\UpdateModelStatuses;
use App\Actions\Article\UpdateTranslateValuesAction;
use App\Actions\PublishedEntity\UpdateActiveEntityAction;
use App\Actions\Translates\CheckKeyExistAction;
use App\Actions\Translates\GetTranslateStatusAction;
use App\Actions\Translates\StoreTranslateValuesAction;
use App\Actions\Translates\SyncTranslateKeyIdValuesAction;
use App\Actions\Translates\SyncTranslateLangValuesAction;
use App\Actions\Translates\TranslateExport;
use App\Actions\Translates\TranslateImport;
use App\Constants\PublishStatusConstant;
use App\Constants\TranslateStatusConstant;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Criterias\Core\OrderByCriteria;
use App\Criterias\Translate\TranslateHasNotValidValueCriteria;
use App\Criterias\Translate\TranslateSearchCriteria;
use App\Events\Admin\UpdateKeyValueEvent;
use App\Models\PublishedEntity;
use App\Models\Translate;
use App\Models\TranslateKey;
use App\Models\TranslateLang;
use App\Repositories\TranslateKeyRepository;
use App\Tasks\CreateTask;
use App\Tasks\DeleteByIdTask;
use App\Tasks\DeleteTask;
use App\Tasks\GetByFieldTask;
use App\Tasks\GetByIdTask;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;
use App\Tasks\MultipleUpdateTask;
use App\Tasks\UpdateTask;
use App\Transformers\Admin\TranslateKeysListTransformer;
use App\Transformers\ArrayTransformer;
use App\Validators\FieldValidator;
use App\Validators\TranslateValidator;

/**
 * Class Uploads
 * @package App\Services
 */
class TranslateService extends ServiceAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function list(array $params = [])
    {
        $search = $params['search'] ?? null;
        $this->manager->task(GetTask::class)
            ->repository(TranslateKeyRepository::class)
            ->method('with', ['langs'])
            ->criteria(new OrderByCriteria('id', 'desc'))
            ->extraMethod('insertLangs')
            ->transformer(TranslateKeysListTransformer::class);

        if (!empty($search)) {
            $this->manager->criteria(new TranslateSearchCriteria($search));
        }

        $needTranslate = $params['need_translate'] ?? null;
        if ($needTranslate) {
            $this->manager->criteria(new TranslateHasNotValidValueCriteria());
        }

        return $this->manager->run();
    }

    /**
     * @return mixed|void|null
     * @throws \Exception
     */
    public function listLangs()
    {
        return $this->manager->task(GetTask::class)
            ->model(TranslateLang::class)
            ->transformer(ArrayTransformer::class)
            ->run();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function storeKey(array $params = []): array
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_CREATE_KEY);

        $this->manager->action(CheckKeyExistAction::class)
            ->arguments(['key' => $params['key']])
            ->run();

        $data = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model(TranslateKey::class)
            ->run();

        $this->manager->action(SyncTranslateKeyIdValuesAction::class)
            ->arguments(['keyId' => $data->id])
            ->run();

        return $this->getKey(['id' => $data->id]);
    }

    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function getKey(array $params = [])
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        return $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $params['id']])
            ->model(TranslateKey::class)
            ->method('with', ['langs'])
            ->transformer(TranslateKeysListTransformer::class)
            ->run();
    }

    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function updateKey(array $params = [])
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_UPDATE_KEY);

        $this->manager->action(CheckKeyExistAction::class)
            ->arguments(['key' => $params['key']])
            ->run();

        $this->manager->task(UpdateTask::class)
            ->arguments(['id' => (int)$params['id'], 'params' => $params])
            ->model(TranslateKey::class)
            ->run();

        return $this->getKey(['id' => (int)$params['id']]);
    }

    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function storeLang(array $params = [])
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_CREATE_LANG);
        $lang = $params['lang'];

        $this->manager->action(SyncTranslateLangValuesAction::class)
            ->arguments(['lang' => $lang])
            ->run();


        $this->manager->task(InsertOrUpdateTask::class)
            ->arguments(['params' => array_intersect_key($params, array_flip(['lang', 'status']))])
            ->model(TranslateLang::class)
            ->run();

        $keys = $this->manager->task(GetTask::class)
            ->model(TranslateKey::class)
            ->run();

        foreach ($keys as $key) {
            event(new UpdateKeyValueEvent($key->id));
        }

        return $this->manager->task(GetByFieldTask::class)
            ->arguments(['field' => 'lang', 'value' => $lang])
            ->model(TranslateLang::class)
            ->transformer(ArrayTransformer::class)
            ->run();
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Exception
     */
    public function storeTranslate(array $params = []): array
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_CREATE_TRANSLATE);

        $keyId = (int)$params['id'];

        $this->manager->action(StoreTranslateValuesAction::class)
            ->arguments(['keyId' => $keyId, 'langsValues' => $params['langs']])
            ->run();

        event(new UpdateKeyValueEvent($keyId));
        return ['data' => true];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function deleteTranslate(array $params = []): array
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_DELETE_TRANSLATE);
        $data = $this->manager->task(DeleteTask::class)
            ->model(Translate::class)
            ->criteria(new EqualCriteria('key_id', $params['id']))
            ->criteria(new EqualCriteria('lang', $params['lang']))
            ->run();
        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function deleteLang(array $params = []): array
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_DELETE_LANG);
        $data = $this->manager->task(DeleteTask::class)
            ->model(TranslateLang::class)
            ->criteria(new EqualCriteria('lang', $params['lang']))
            ->run();
        $this->manager->task(DeleteTask::class)
            ->model(Translate::class)
            ->criteria(new EqualCriteria('lang', $params['lang']))
            ->run();

        $keys = $this->manager->task(GetTask::class)
            ->model(TranslateKey::class)
            ->run();

        foreach ($keys as $key) {
            event(new UpdateKeyValueEvent($key->id));
        }

        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function deleteKey(array $params = []): array
    {
        app(TranslateValidator::class)->with($params)->passesOrFail(TranslateValidator::RULE_DELETE_KEY);
        $data = $this->manager->task(DeleteByIdTask::class)
            ->arguments(['id' => (int)$params['id']])
            ->model(TranslateKey::class)
            ->run();
        $keys = $this->manager->task(GetTask::class)
            ->model(TranslateKey::class)
            ->run();

        foreach ($keys as $key) {
            event(new UpdateKeyValueEvent($key->id));
        }
        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function export(array $params = [])
    {
        return $this->manager->action(TranslateExport::class)
            ->arguments(['params' => $params])
            ->run();
    }

    /**
     * @param $fileId
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function import($fileId, array $params = []): array
    {
        $data = $this->manager->action(TranslateImport::class)
            ->arguments(['fileId' => $fileId, 'params' => $params])
            ->run();
        return ['data' => $data];
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Exception
     */
    public function send(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);

        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => (int)$params['id']])->run();

        if ($model->translate_status == TranslateStatusConstant::TRANSLATED) {
            throw new \Exception('Текст еще не переведен', 422);
        }

        $this->manager->action(SendToTranslateArticleAction::class)
            ->arguments(['model' => $model])->run();

        $this->manager->action(UpdateModelStatuses::class)
            ->arguments(['model' => $model])->run();

        return ['data' => true];
    }

    /**
     * @param array $params
     * @return false[]
     * @throws \Exception
     */
    public function approve(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);

        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => (int)$params['id']])->run();

        $bindKeys = $this->manager->action(GetBindKeyValuesAction::class)
            ->arguments(['model' => $model])
            ->run();

        $status = $this->manager->action(GetTranslateStatusAction::class)
            ->arguments(['bindKeys' => $bindKeys])
            ->run();

        if ($status == 'need_update') {
            $this->manager->action(SendToTranslateArticleAction::class)
                ->arguments(['model' => $model])->run();

            $keys = $model->keys()->get();
            $ids = array_column($keys->toArray(), 'id');

            if (!empty($ids)) {
                $languages = $this->manager->task(GetTask::class)
                    ->criteria(new EqualCriteria('status',1))
                    ->model(TranslateLang::class)
                    ->run()->toArray();
                $languages = array_column($languages, 'lang');

                $this->manager->task(MultipleUpdateTask::class)
                    ->arguments(['params' => ['is_valid' => 1]])
                    ->criteria(new InCriteria('key_id', $ids))
                    ->criteria(new InCriteria('lang', $languages))
                    ->model(Translate::class)->run();
            }
        }

        $this->manager->action(UpdateTranslateValuesAction::class)
            ->arguments(['model' => $model])->run();

        $this->manager->action(UpdateModelStatuses::class)
            ->arguments(['model' => $model])->run();

        return ['data' => true];
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Exception
     */
    public function publish(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);

        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => (int)$params['id']])->run();
        $entityType = $model->getEntityType();
        if ($model->translate_status != TranslateStatusConstant::TRANSLATED) {
            throw new \Exception('Текст на переводе', 422);
        }

        if ($model->publish_status == PublishStatusConstant::PUBLISHED) {
            throw new \Exception('Текст уже опубликован', 422);
        }

        $this->manager->action(PublishArticleAction::class)
            ->arguments(['model' => $model])->run();

        $this->manager->action(UpdateModelStatuses::class)
            ->arguments(['model' => $model])->run();

        $this->manager->action(UpdateActiveEntityAction::class)->arguments([$entityType])->run();

        return ['data' => true];
    }

    /**
     * @param array $params
     * @return bool[]
     * @throws \Exception
     */
    public function unpublished(array $params = []): array
    {
        app(FieldValidator::class)->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => (int)$params['id']])->run();
        $entityType = $model->getEntityType();
        if ($model->publish_status == PublishStatusConstant::UNPUBLISHED) {
            throw new \Exception('publish_status не должен быть равен ' . PublishStatusConstant::UNPUBLISHED, 422);
        }

        $this->manager->task(DeleteTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_id', $model->id))
            ->criteria(new EqualCriteria('entity_type', $model->getEntityType()))
            ->run();

        $this->manager->action(UpdateModelStatuses::class)
            ->arguments(['model' => $model])->run();

        $this->manager->action(UpdateActiveEntityAction::class)->arguments([$entityType])->run();

        return ['data' => true];
    }
}
