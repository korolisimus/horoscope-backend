<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Criterias\Core\EqualCriteria;
use App\Models\Compare;
use App\Tasks\GetFirstTask;

class CompareService extends ServiceAbstract
{
    protected string $model = Compare::class;

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function get(array $params = []): array
    {
        $data = $this->manager->task(GetFirstTask::class)
            ->model($this->model)
            ->criteria(new EqualCriteria('type', 'compare'))
            ->method('with', ['publishedEntity'])
            ->transformer($this->singleTransformer)
            ->run();
        if (empty($data['data'])) {
            return $this->create($params);
        }
        return $data;
    }
}