<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Criterias\Core\EqualCriteria;
use App\Models\PublishedEntity;
use App\Tasks\GetFirstTask;
use App\Tasks\MultipleUpdateTask;
use App\Tasks\UpdateTask;
use App\Validators\FieldValidator;

class PublishedEntityService extends ServiceAbstract
{
    /**
     * @param array $params
     * @return bool[]
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateStatus(array $params = []): array
    {
        /** @var FieldValidator $validator */
        $validator = app(FieldValidator::class);
        $validator->with($params)->passesOrFail(FieldValidator::RULE_ID);
        $validator->with($params)->passesOrFail(FieldValidator::RULE_STATUS);
        $id = $params['id'];
        $entity = $this->manager->task(GetFirstTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_id', $id))->run();

        if (!empty($entity)) {
            $this->manager->task(MultipleUpdateTask::class)
                ->model(PublishedEntity::class)
                ->criteria(new EqualCriteria('entity_type', $entity->entity_type))
                ->arguments([['status' => 0]])
                ->run();

            $this->manager->task(UpdateTask::class)
                ->arguments([$entity->id, ['status' => 1, 'use_date' => date('Y-m-d H:i:s', time())]])
                ->model(PublishedEntity::class)
                ->run();
        }
        return ['data' => true];
    }
}