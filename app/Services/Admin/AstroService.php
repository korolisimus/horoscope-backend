<?php declare(strict_types=1);

namespace App\Services\Admin;

use App\Models\Astro;

class AstroService extends ServiceAbstract
{
    protected string $model = Astro::class;
}