<?php

namespace App\Services\Utils;

use App\Criterias\Core\IsNullCriteria;
use App\Models\TranslateKey;
use App\Tasks\GetTask;
use App\Traits\ManagerTrait;

/**
 * Class Base
 * @package App\Services
 */
class Base
{
    use ManagerTrait;

    /**
     * @var array
     */
    private array $languages;

    /**
     * @var array
     */
    private array $countries;

    /**
     * @return mixed
     */
    public function languages(): array
    {
        if (empty($this->languages)) {
            $path = storage_path('app/locales/langs.json');
            $this->languages = json_decode(file_get_contents($path), true);
        }
        return $this->languages;
    }

    /**
     * @return mixed
     */
    public function countries(): array
    {
        if (empty($this->countries)) {
            $path = storage_path('app/locales/countries.json');
            $this->countries = json_decode(file_get_contents($path), true);
        }
        return $this->countries;
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function translates(array $params = []): array
    {
        $lang = $params['lang'] ?? 'en';
        $translates = $this->manager->task(GetTask::class)
            ->criteria(new IsNullCriteria('type'))
            ->model(TranslateKey::class)
            ->method('with', ['langs'])
            ->run()->toArray();
        $result = [];
        foreach ($translates as $translate) {
            $langs = $translate['langs'];
            $index = array_search($lang, array_column($langs, 'lang'));
            if ($index !== false) {
                $result[$translate['key']] = $langs[$index]['value'];
            }
        }
        return ['data' => $result];
    }
}
