<?php declare(strict_types=1);

namespace App\Actions\Utils;

use App\Actions\ActionAbstract;

class DataIteratorAction extends ActionAbstract
{
    public function run($data, $callback): \Generator
    {
        if (is_array($data)) {
            foreach ($data as $item) {
                yield from $this->run($item, $callback);
            }
        } else {
            if ($callback($data)) {
                yield $data;
            }
        }
    }
}
