<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;

class ParseTranslatePathAction extends ActionAbstract
{
    /**
     * @param string $path
     * @return array
     */
    public function run(string $path): array
    {
        if (empty($path)) return [];
        return array_map(function ($part) {
            $matches = [];
            if (preg_match("/([^\[]*)\[(.*)\]/", $part, $matches)) {
                return [
                    'depth' => $matches[1],
                    'key' => $matches[2]
                ];
            } else {
                return ['key' => $part];
            }
        }, explode('.', $path));
    }
}
