<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\ModelAbstract;

/**
 * Class GetDataKeysAction
 * @package App\Actions\Translates
 */
class GetKeysAction extends ActionAbstract
{
    /**
     * @param \App\Models\ModelAbstract $model
     * @param array $info
     * @return array
     */
    public function run(ModelAbstract $model, array $info): array
    {
        $currentKey = $model->id . "_";
        $keys = [];
        $data = $model->toArray();
        $this->getKeys($data, $info, $currentKey, $keys);
        return $keys;
    }

    /**
     * @param $data
     * @param $info
     * @param $currentKey
     * @param $keys
     */
    private function getKeys($data, $info, $currentKey, &$keys)
    {
        $meta = array_shift($info);
        $path = $meta['path'] ?? null;
        $key = $meta['key'] ?? null;

        if (!empty($path)) {
            $data = $data[$path];
            foreach ($data as $item) {
                $tmpKey = $currentKey . $item[$key] . "_";
                if (!empty($info)) {
                    $this->getKeys($item, $info, $tmpKey, $keys);
                } else {
                    if (!empty($item[$key])) {
                        $keys[] = [
                            'key' => $currentKey,
                            'value' => $item[$key]
                        ];
                    }
                }
            }
        } else {
            $currentKey .= $key;
            if (!empty($data[$key])) {
                $keys[] = [
                    'key' => $currentKey,
                    'value' => $data[$key]
                ];
            }

        }
    }
}