<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Models\Translate;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;

/**
 * Class SyncTranslateLangValuesAction
 * @package App\Actions\Translates
 */
class SyncTranslateLangValuesAction extends ActionAbstract
{
    /**
     * @param string $lang
     * @throws \Exception
     */
    public function run(string $lang)
    {
        $values = $this->manager->task(GetTask::class)
            ->model(Translate::class)
            ->criteria(new EqualCriteria('lang', 'ru'))
            ->run();

        $update = [];
        foreach ($values as $value) {
            $update[] = [
                'key_id' => $value->key_id,
                'lang' => $lang
            ];
        }

        if (!empty($values)) {
            $this->manager->task(InsertOrUpdateTask::class)
                ->arguments(['params' => $update])
                ->model(Translate::class)
                ->run();
        }
    }
}