<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;

class TranslatePublishEntityAction extends ActionAbstract
{
    public function run($data, string $lang)
    {
        $this->process($data, $lang);
        return $data;
    }

    private function process(&$data, string $lang)
    {
        if (is_array($data)) {
            if (!empty($data['translates'])) {
                foreach ($data['translates'] as $field => $langs) {
                    if (!empty($langs[$lang])) {
                        $data[$field] = $langs[$lang];
                    } else {
                        $data[$field] = '';
                    }
                }
            } else {
                foreach ($data as &$items) {
                    $this->process($items, $lang);
                }
            }

            unset($data['translates']);
            unset($data['translate_info']);
        }
    }
}