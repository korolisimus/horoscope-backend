<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\TranslateKey;
use App\Tasks\GetByFieldTask;
use Exception;

class CheckKeyExistAction extends ActionAbstract
{
    /**
     * @param string $key
     * @throws \Exception
     */
    public function run(string $key)
    {
        $find = $this->manager->task(GetByFieldTask::class)
            ->arguments(['field' => 'key', 'value' => $key])
            ->model(TranslateKey::class)
            ->run();

        if (!empty($find)) {
            throw new Exception('Ключ существует', 400);
        }
    }
}