<?php declare(strict_types=1);

namespace App\Actions\Translates;


use App\Actions\ActionAbstract;
use App\Events\Admin\UpdateKeyValueEvent;
use App\Models\File;
use App\Models\Translate;
use App\Models\TranslateKey;
use App\Tasks\GetByFieldTask;
use App\Tasks\GetByIdTask;
use App\Tasks\InsertOrUpdateTask;
use Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;

class TranslateImport extends ActionAbstract
{
    /**
     * @param $fileId
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    public function run($fileId, array $params = []): bool
    {
        $file = $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $fileId])
            ->model(File::class)->run();
        $path = $file->path;
        $lang = $params['lang'] ?? null;
        if (empty($lang)) {
            throw new Exception('Язык не указан', 200);
        }
        $path = storage_path('app/' . $path);

        $spreadsheet = IOFactory::load($path);
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = [];
        foreach ($worksheet->getRowIterator() as $i => $item) {
            $cellIterator = $item->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $cell) {
                $v = $cell->getValue();
                if (empty($v)) {
                    continue;
                }
                $cells[] = $v;
            }
            if ($i == 1) {
                continue;
            }
            if (!empty($cells)) {
                $rows[] = $cells;
            }
        }
        $update = [];
        $keys = [];
        foreach ($rows as $row) {
            $key = $this->manager->task(GetByFieldTask::class)
                ->arguments(['field' => 'key', 'value' => $row[0]])
                ->model(TranslateKey::class)->run();
            $value = $row[2] ?? null;
            if (empty($key) || empty($value)) {
                continue;
            }
            $keys[] = $key->id;
            $update[] = [
                'key_id' => $key->id,
                'lang' => $lang,
                'value' => $value,
                'is_valid' => 1
            ];
        }

        $this->manager->task(InsertOrUpdateTask::class)
            ->model(Translate::class)
            ->arguments(['params' => $update])->run();

        foreach ($keys as $key) {
            event(new UpdateKeyValueEvent($key));
        }

        return true;
    }
}