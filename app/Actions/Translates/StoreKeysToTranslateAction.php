<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\TranslateKey;
use App\Tasks\InsertOrUpdateTask;

class StoreKeysToTranslateAction extends ActionAbstract
{
    /**
     * @param array $keys
     * @return mixed|void|null
     * @throws \Exception
     */
    public function run(array $keys = [])
    {
        return $this->manager->task(InsertOrUpdateTask::class)
            ->arguments(['params' => array_map(function ($item) {
                return [
                    'key' => $item,
                    'type' => 'attribute'
                ];
            }, $keys)])
            ->model(TranslateKey::class)
            ->run();
    }
}