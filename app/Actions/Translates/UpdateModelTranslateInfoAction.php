<?php declare(strict_types=1);

namespace App\Actions\Translates;


use App\Actions\ActionAbstract;
use App\Models\TranslateModelInterface;
use App\Tasks\FirstOrCreateTask;

class UpdateModelTranslateInfoAction extends ActionAbstract
{
    /**
     * @param \App\Models\TranslateModelInterface $model
     * @return array
     * @throws \Exception
     */
    public function run(TranslateModelInterface $model): array
    {

        $data = $model->toArray();

        $this->clearTranslateInfo($data);
        $bindKeys = [];
        $translateFields = $model::getTranslateFields();
        foreach ($translateFields as $translateField) {
            $path = $translateField['path'] ?? null;
            if (empty($path)) {
                continue;
            }
            $path = $this->manager->action(ParseTranslatePathAction::class)
                ->arguments(['path' => $path])->run();

            $currentKey = $data['id'];
            $this->process($data, $path, $currentKey, $bindKeys, $translateField);
        }

        $model->translate_info = $data['translate_info'] ?? null;
        $model->data = $data['data'] ?? null;
        $model->save();
        return $bindKeys;
    }

    private function clearTranslateInfo(&$data)
    {
        if (is_array($data)) {
            $data = array_filter($data, function ($value, $key) {
                return $key !== 'translate_info';
            }, ARRAY_FILTER_USE_BOTH);

            foreach ($data as &$item) {
                $this->clearTranslateInfo($item);
            }
        }
    }

    /**
     * @param $data
     * @param $pathInfo
     * @param $currentKey
     * @param $bindKeys
     * @param $translateField
     * @throws \Exception
     */
    private function process(&$data, $pathInfo, $currentKey, &$bindKeys, $translateField)
    {
        $info = array_shift($pathInfo);
        $depth = $info['depth'] ?? null;
        $key = $info['key'];
        if (!empty($depth)) {
            if (!empty($data[$depth])) {
                foreach ($data[$depth] as &$depthItem) {
                    $tmpKey = $currentKey . "_" . $depthItem[$key];
                    $this->process($depthItem, $pathInfo, $tmpKey, $bindKeys, $translateField);
                }
            }
        } else {
            $tmpKey = $currentKey . "_" . $key;
            $value = $data[$key];
            $type = $translateField['type'];
            if (!empty($value)) {
                $value = strip_tags($value);
            }
            if (empty($value) || is_numeric($value)) return;
            switch ($type) {
                case 'key':
                    if (empty($data['translate_info'])) {
                        $data['translate_info'] = [];
                    }
                    $data['translate_info'][$key] = $tmpKey;
                    $bindKeys[$tmpKey] = $value;
                    break;
                case 'unique':
                    $model = $translateField['model'];
                    $modelField = $translateField['modelField'];
                    $params = [$modelField => $value];
                    $params['type'] = $model::getType();
                    $uniqueModel = $this->manager->task(FirstOrCreateTask::class)
                        ->arguments(['params' => $params])
                        ->model($model)->run();
                    $uniqueType = $uniqueModel->type;
                    $tmpKey = $uniqueModel->id . "_" . $uniqueType . "_" . $modelField;
                    $data['translate_info'][$key] = $tmpKey;
                    $bindKeys[$tmpKey] = $value;
                    break;
            }
        }
    }

}