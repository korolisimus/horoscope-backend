<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Constants\TranslateStatusConstant;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Models\TranslateKey;
use App\Models\TranslateLang;
use App\Tasks\GetTask;

class GetTranslateStatusAction extends ActionAbstract
{
    private array $existLangs;

    /**
     * @param array $bindKeys
     * @return string
     * @throws \Exception
     */
    public function run(array $bindKeys): string
    {
        $langs = $this->manager->task(GetTask::class)
            ->criteria(new EqualCriteria('status', 1))
            ->model(TranslateLang::class)->run();
        $this->existLangs = array_column($langs->toArray(), 'lang');

        $keyNames = array_keys($bindKeys);

        $keyObjects = $this->manager->task(GetTask::class)
            ->criteria(new InCriteria('key', $keyNames))
            ->method('with', ['langs', 'langRu'])
            ->model(TranslateKey::class)
            ->run()->toArray();

        $hasEmpty = false;
        $hasInvalid = false;
        $hasDifferent = false;

        $result = TranslateStatusConstant::NEED_TRANSLATE;
        if (count($keyNames) != count($keyObjects)) {
            return $result;
        }

        foreach ($keyObjects as $object) {
            $this->check($object, $hasEmpty, $hasInvalid, $hasDifferent);
        }

        if (!$hasEmpty && $hasDifferent) {
            $result = TranslateStatusConstant::NEED_UPDATE;
        } else if($hasEmpty)
        {
            $result = TranslateStatusConstant::PROCESS_TRANSLATE;
        }
        /* else if ($hasInvalid) {
            if (!$hasDifferent) {
                $result = TranslateStatusConstant::PROCESS_TRANSLATE;
            }
        }*/ else {
            $result = TranslateStatusConstant::TRANSLATED;
        }

        return $result;
    }

    private function check($translateKey, &$has_empty, &$has_invalid, &$has_different)
    {
        $translateKey['langs'] = array_filter($translateKey['langs'], function ($item) {
            return in_array($item['lang'], $this->existLangs);
        });
        $values = array_column($translateKey['langs'], 'value');
        $res = array_filter($values, function ($item) {
            return empty($item);
        });

        if (!empty($res)) {
            $has_empty = true;
        }

        /*$validate = array_search(0, array_column($translateKey['langs'], 'is_valid'));
        if ($validate !== false) {
            $has_invalid = true;
        }*/

        $lang_ru_value = $translateKey['lang_ru']['value'];
        $memory_value = $translateKey['lang_ru']['memory_value'] ?? null;

        if ($lang_ru_value != $memory_value) {
            $has_different = true;
        }
    }
}