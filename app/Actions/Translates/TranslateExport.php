<?php declare(strict_types=1);

namespace App\Actions\Translates;


use App\Actions\ActionAbstract;
use App\Services\Admin\TranslateService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class TranslateExport extends ActionAbstract
{
    /**
     * @param array $params
     * @return string
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function run(array $params = []): string
    {
        $lang_from = $params['lang_from'] ?? 'ru';
        $handle = new Spreadsheet();
        $file = $handle->getActiveSheet();
        $index = 1;
        $file->setCellValue('A' . $index, 'key');
        $file->setCellValue('B' . $index, 'original');
        $file->setCellValue('C' . $index, 'translate');

        $data = [];

        //todo replace
        /** @var TranslateService $service */
        $service = app(TranslateService::class);
        $items = $service->list(['need_translate' => true]);
        //$items = $this->call(Get::class, [['need_translate' => true]]);

        foreach ($items['data'] as $item) {
            $value = $item['langs'][$lang_from]['value'];
            if (!empty($value)) {
                $data[] = [$item['key'], $value];
            }
        }

        foreach ($data as $row) {
            $index++;
            foreach (['A', 'B'] as $i => $column) {
                $file->setCellValue($column . $index, $row[$i]);
            }
        }
        $file_path = storage_path('app/files/' . time() . "_export.xlsx");
        $writer_trans = new Xlsx($handle);
        $writer_trans->save($file_path);
        return $file_path;
    }
}