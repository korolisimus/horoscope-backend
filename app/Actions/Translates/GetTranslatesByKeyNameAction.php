<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Criterias\Core\InCriteria;
use App\Models\TranslateKey;
use App\Tasks\GetTask;

class GetTranslatesByKeyNameAction extends ActionAbstract
{
    /**
     * @param array $keys
     * @param string $lang
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $keys, string $lang): array
    {
        $translates = $this->manager->task(GetTask::class)
            ->model(TranslateKey::class)
            ->criteria(new InCriteria('key', $keys))
            ->method('with', ['langs' => function($query) use ($lang){
                $query->where('lang','=',$lang);
            }])->run()->toArray();
        $result = [];

        array_map(function($item) use (&$result) {
            $result[$item['key']] = $item['langs'][0]['value'];
        }, $translates);

        foreach ($keys as $key){
            if(empty($result[$key])){
                $result[$key] = '';
            }
        }
        return $result;
    }
}
