<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\Translate;
use App\Models\TranslateLang;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;

class StoreTranslateValuesAction extends ActionAbstract
{
    /**
     * @param int $keyId
     * @param array $langsValues
     * @return bool
     * @throws \Exception
     */
    public function run(int $keyId, array $langsValues = []): bool
    {
        $langs = $this->manager->task(GetTask::class)
            ->model(TranslateLang::class)
            ->run()->toArray();
        $existLangs = array_flip(array_column($langs, 'lang'));

        $update = [];
        foreach ($langsValues as $langKey => $item) {
            if (!isset($existLangs[$langKey])) continue;
            if ($langKey == 'ru') {
                $item['is_valid'] = 1;
            } else if (empty($item['value'])) {
                $item['is_valid'] = 0;
            }
            $update[] = [
                'key_id' => $keyId,
                'value' => $item['value'],
                'is_valid' => $item['is_valid'],
                'lang' => $langKey
            ];
        }
        if (!empty($update)) {
            $this->manager->task(InsertOrUpdateTask::class)
                ->arguments(['params' => $update])
                ->model(Translate::class)
                ->run();
        }
        return true;
    }
}