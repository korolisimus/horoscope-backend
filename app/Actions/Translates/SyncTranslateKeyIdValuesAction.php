<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\Translate;
use App\Models\TranslateLang;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;

class SyncTranslateKeyIdValuesAction extends ActionAbstract
{
    /**
     * @param int $keyId
     * @return bool
     * @throws \Exception
     */
    public function run(int $keyId): bool
    {
        $existLangs = $this->manager->task(GetTask::class)
            ->model(TranslateLang::class)
            ->run();

        $update = [];

        foreach ($existLangs as $lang) {
            $update[] = [
                'key_id' => $keyId,
                'lang' => $lang['lang'],
                'is_valid' => $lang['lang'] == 'ru' ? 1 : 0
            ];
        }

        if (!empty($update)) {
            $this->manager->task(InsertOrUpdateTask::class)
                ->arguments(['params' => $update])
                ->model(Translate::class)
                ->run();
        }

        return true;
    }
}