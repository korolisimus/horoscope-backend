<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Actions\Article\GetBindKeyValuesAction;
use App\Criterias\Core\LikeBeginCriteria;
use App\Criterias\Core\NotInCriteria;
use App\Models\TranslateKey;
use App\Models\TranslateModelInterface;
use App\Tasks\DeleteTask;

class DeleteUnusedKeysAction extends ActionAbstract
{
    /**
     * @param \App\Models\TranslateModelInterface $model
     * @throws \Exception
     */
    public function run(TranslateModelInterface $model)
    {
        $bindKeys = $this->manager->action(GetBindKeyValuesAction::class)
            ->arguments(['model' => $model])->run();
        $this->manager->task(DeleteTask::class)
            ->model(TranslateKey::class)
            ->criteria(new NotInCriteria('key', array_keys($bindKeys)))
            ->criteria(new LikeBeginCriteria('key', $model->id . "_"))
            ->run();
    }
}