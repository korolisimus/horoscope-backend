<?php declare(strict_types=1);

namespace App\Actions\Translates;

use App\Actions\ActionAbstract;
use App\Models\TranslateLang;
use App\Tasks\GetTask;

class GetExistLangsAction extends ActionAbstract
{
    /**
     * @return mixed|void|null
     * @throws \Exception
     */
    public function run()
    {
        return $this->manager->task(GetTask::class)
            ->model(TranslateLang::class)
            ->run();
    }
}