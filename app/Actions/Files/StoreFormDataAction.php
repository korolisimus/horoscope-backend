<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;
use App\Actions\Storage\StoreThumbnailVideo;
use App\Models\File;
use App\Tasks\CreateTask;
use App\Tasks\GetByFieldTask;

/**
 * Class StoreFormData
 * @package App\Actions\Files
 */
class StoreFormDataAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function run(array $params = []): array
    {
        $result = [];
        $iterator = $this->manager->action(MakeFormDataIteratorAction::class)
            ->arguments([$params])->run();
        foreach ($iterator as $item) {
            if ($id = $this->process($item)) {
                $result[] = $id;
            }
        }
        return $result;
    }

    /**
     * @param $item
     * @return mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function process($item)
    {

        $file = $this->manager->task(GetByFieldTask::class)
            ->arguments(['field' => 'hash', 'value' => md5_file($item->getRealPath())])
            ->model(File::class)
            ->run();
        if (empty($file)) {
            $data = $this->manager->action(\App\Actions\Storage\StoreFormData::class)
                ->arguments(['file' => $item])->run();
            if ($data['type'] == 'video') {
                $thumbnail_info = $this->manager->action(StoreThumbnailVideo::class)
                    ->arguments(['data' => $data])->run();
                if (!empty($thumbnail_info)) {
                    $thumbnail = $this->manager->task(CreateTask::class)
                        ->arguments(['params' => $thumbnail_info])
                        ->model(File::class)
                        ->run();
                    $data['thumbnail'] = $thumbnail->id;
                }
            }
            $file = $this->manager->task(CreateTask::class)
                ->arguments(['params' => $data])
                ->model(File::class)
                ->run();
        }
        return $file->id;
    }
}
