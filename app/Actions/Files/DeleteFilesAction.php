<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;
use App\Models\File;
use Illuminate\Database\Eloquent\Collection;

class DeleteFilesAction extends ActionAbstract
{
    /**
     * @param \Illuminate\Database\Eloquent\Collection $files
     * @throws \Exception
     */
    public function run(Collection $files)
    {
        foreach ($files as $file) {
            if ($file instanceof File) {
                $pathList = $file->getPathList();
                foreach ($pathList as $path) {
                    if (!empty($file->{$path})) {
                        $filePath = storage_path('app/' . $file->{$path});
                        @unlink($filePath);
                    }
                }
                $file->delete();
            }
        }
    }
}