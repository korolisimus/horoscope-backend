<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;
use App\Actions\Utils\DataIteratorAction;
use Illuminate\Http\UploadedFile;

class MakeFormDataIteratorAction extends ActionAbstract
{
    /**
     * @param array $data
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $data)
    {
        return $this->manager->action(DataIteratorAction::class)
            ->arguments([$data, function ($item) {
                return $item instanceof UploadedFile;
            }])->run();
    }
}
