<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;
use App\Models\File;
use App\Tasks\CreateTask;
use App\Tasks\GetByFieldTask;
use Throwable;

class StoreBase64Action extends ActionAbstract
{
    /**
     * @param array $params
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = []): array
    {
        $iterator = $this->manager->action(MakeBase64DataIteratorAction::class)
            ->arguments([$params])->run();
        $results = [];
        foreach ($iterator as $file) {
            if ($id = $this->process($file)) {
                $results[] = $id;
            }
        }
        return $results;
    }

    private function process($item)
    {
        $id = null;
        try {
            $hash = $this->getHash($item);
            $file = $this->manager->task(GetByFieldTask::class)
                ->arguments(['field' => 'hash', 'value' => $hash])
                ->model(File::class)
                ->run();

            if (empty($file)) {
                $data = $this->manager->action(\App\Actions\Storage\StoreBase64::class)
                    ->arguments(['file' => $item])
                    ->run();

                $file = $this->manager->task(CreateTask::class)
                    ->arguments(['params' => $data])
                    ->model(File::class)
                    ->run();

            }
            $id = $file->id;
        } catch (Throwable $e) {
            info('File not uploaded: ' . $e->getMessage());
        }
        return $id;
    }

    /**
     * @param $item
     * @return string
     */
    private function getHash($item): string
    {
        $content = substr($item, strpos($item, ',') + 1);
        $content = base64_decode($content);
        return md5($content);
    }
}