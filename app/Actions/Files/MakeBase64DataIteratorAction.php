<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;
use App\Actions\Utils\DataIteratorAction;

class MakeBase64DataIteratorAction extends ActionAbstract
{
    /**
     * @param array $data
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $data)
    {
        return $this->manager->action(DataIteratorAction::class)
            ->arguments([$data, function ($item) {
                return preg_match('/^data:image\/(\w+);base64,/', (string)$item);
            }])->run();
    }
}
