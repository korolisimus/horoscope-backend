<?php declare(strict_types=1);

namespace App\Actions\Files;

use App\Actions\ActionAbstract;

class Base64IteratorAction extends ActionAbstract
{
    public function run($params,$callback): \Generator
    {
        if (is_array($params)) {
            foreach ($params as $item) {
                yield from $this->run($item, $callback);
            }
        } else {
            if ($callback($params)) {
                yield $params;
            }
        }

    }
}