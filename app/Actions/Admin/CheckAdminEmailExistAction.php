<?php declare(strict_types=1);

namespace App\Actions\Admin;

use App\Actions\ActionAbstract;
use App\Models\Admin;
use App\Tasks\GetByFieldTask;
use Exception;

class CheckAdminEmailExistAction extends ActionAbstract
{
    /**
     * @param array $params
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function run(array $params = []){
        if (!empty($params['email'])) {
            $find = $this->manager->task(GetByFieldTask::class)
                ->arguments(['field' => 'email', 'value' => $params['email']])
                ->model(Admin::class)
                ->run();
            if (!empty($find) && $find->id != $params['id']) {
                throw new Exception('ADMIN_EMAIL_ALREADY_EXIST', 400);
            }
        }
    }
}