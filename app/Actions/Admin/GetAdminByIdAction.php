<?php declare(strict_types=1);


namespace App\Actions\Admin;

use App\Actions\ActionAbstract;
use App\Models\Admin;
use App\Tasks\GetByIdTask;
use App\Transformers\Admin\AdminTransformer;

class GetAdminByIdAction extends ActionAbstract
{
    /**
     * @param int $id
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(int $id)
    {
        return $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $id])
            ->model(Admin::class)
            ->transformer(AdminTransformer::class)
            ->run();
    }
}