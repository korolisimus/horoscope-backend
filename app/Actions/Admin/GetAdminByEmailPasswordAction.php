<?php declare(strict_types=1);

namespace App\Actions\Admin;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Models\Admin;
use App\Tasks\GetFirstTask;
use App\Validators\AdminValidator;
use Exception;

class GetAdminByEmailPasswordAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        app(AdminValidator::class)->with($params)->passesOrFail(AdminValidator::RULE_LOGIN);
        $admin = $this->manager->task(GetFirstTask::class)
            ->criteria(new EqualCriteria('email', $params['email']))
            ->criteria(new EqualCriteria('password', $params['password']))
            ->model(Admin::class)
            ->run();

        if (empty($admin)) {
            throw new Exception('Неверный логин или пароль.', 400);
        }
        return $admin;
    }
}
