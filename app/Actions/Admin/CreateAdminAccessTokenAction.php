<?php declare(strict_types=1);

namespace App\Actions\Admin;

use App\Actions\AccessToken\CreateAccessTokenAction;
use App\Actions\ActionAbstract;
use App\Models\Admin;

class CreateAdminAccessTokenAction extends ActionAbstract
{
    /**
     * @param \App\Models\Admin $admin
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(Admin $admin)
    {
        return $this->manager->action(CreateAccessTokenAction::class)
            ->arguments(['id' => $admin->id, 'type' => 'admin'])
            ->run();
    }
}
