<?php declare(strict_types=1);

namespace App\Actions\Admin;

use App\Actions\ActionAbstract;
use App\Models\Admin;
use App\Tasks\UpdateTask;

class UpdateAdminByIdAction extends ActionAbstract
{
    /**
     * @param int $id
     * @param array $params
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(int $id, array $params = []){
        return $this->manager->task(UpdateTask::class)
            ->arguments([$id, $params])
            ->model(Admin::class)
            ->run();
    }
}