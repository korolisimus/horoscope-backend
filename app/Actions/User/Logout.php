<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;

/**
 * Class Logout
 * @package App\Actions\User
 */
class Logout extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|null
     */
    public function run(array $params = [])
    {
        $user = app('auth')->auth($params);
        return $this->call(Delete::class, [], [
            ['addCriteria' => [new EqualCriteria('token', $user->token)]]
        ]);
    }
}
