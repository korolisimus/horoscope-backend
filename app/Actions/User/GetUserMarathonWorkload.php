<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Tasks\User\GetUserSettingWithDefault;

/**
 * Class GetUserMarathonWorkload
 * @package App\Actions\User
 */
class GetUserMarathonWorkload extends Action
{
    /**
     * @param int $user_id
     * @param int $marathon_id
     * @return string
     */
    public function run(int $user_id, int $marathon_id)
    {
        $settings = $this->call(GetUserSettingWithDefault::class, [
            ['user_id' => $user_id, 'entity_id' => $marathon_id, 'type' => 'marathon']
        ], [], [
            ['first' => []],
            ['toArray' => []]
        ]);
        return $settings['settings']['workload'] ?? app('auth')->auth()->workload;
    }
}
