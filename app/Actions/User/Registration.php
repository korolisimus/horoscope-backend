<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Models\AccessToken;
use App\Models\User;
use App\Tasks\CreateTask;
use App\Validators\App\UserValidator;

/**
 * Class Registration
 * @package App\Actions\User
 */
class Registration extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function run(array $params = [])
    {
        app(UserValidator::class)->with($params)->passesOrFail(UserValidator::RULE_REGISTRATION);
        $info = $this->manager->action(GetUserInfoAction::class);
        $params['country'] = strtolower($info->country ?? 'by');
        $params['timezone'] = strtolower($info->timezone ?? 'Europe/Minsk');
        $user = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model(User::class)->run();
        return $this->manager->task(CreateTask::class)
            ->arguments(['entity' => $user->id, 'type' => 'user'])
            ->model(AccessToken::class)
            ->run();
    }
}
