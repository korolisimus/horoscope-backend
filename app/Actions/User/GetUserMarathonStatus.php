<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Actions\UserEvent\GetUserEvents;
use App\Tasks\Tournament\Get;

/**
 * Class GetUserMarathonStatus
 * @package App\Actions\User
 */
class GetUserMarathonStatus extends Action
{
    /**
     * @param int $marathon_id
     * @param string $access
     * @return string
     */
    public function run(int $marathon_id, string $access): string
    {
        //$events = app('user_events')->run($marathon_id);
        $event_status = $events['marathon'][$marathon_id] ?? 'not_started';
        $status = ['not_started', 'started', 'wait_results', 'finished'];
        if ($access == 'prize') {
            //todo implement this part

            //get user tournament
            //wait_results = finished + tournament is not waiting results
            //finished = finished + tournament is finished
        }
        return $event_status;
    }
}
