<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Actions\AccessToken\Create;
use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Exceptions\WrongLoginOrPassword;
use App\Tasks\User\Get;

class Login extends ActionAbstract
{

    public function run(array $params = [])
    {
        $email = $params['email'] ?? null;
        $password = $params['password'] ?? null;
        $user = $this->call(Get::class, [], [
            ['addCriteria' => [new EqualCriteria('email', $email)]],
            ['addCriteria' => [new EqualCriteria('password', $password)]],
        ], [
            ['first' => []]
        ]);

        if (empty($user)) {
            throw new WrongLoginOrPassword('ADMIN_WRONG_LOGIN_OR_PASSWORD', 400);
        }

        return $this->call(Create::class, [$user->id, 'user']);
    }
}