<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Tasks\User\GetUserSettingWithDefault;

/**
 * Class GetUserSettings
 * @package App\Actions\User
 */
class GetUserSettings extends Action
{
    /**
     * @param int $user_id
     * @param int $entity_id
     * @param string $type
     * @return mixed
     */
    public function run(int $user_id, int $entity_id, string $type){
        $settings = $this->call(GetUserSettingWithDefault::class, [
            ['user_id' => $user_id, 'type' => $type, 'entity_id' => $entity_id]
        ], [], [
            ['first' => []],
            ['toArray' => []]
        ]);
        return $settings['settings'];
    }
}
