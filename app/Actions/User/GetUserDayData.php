<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;

/**
 * Class GetUserDayData
 * @package App\Actions\User
 */
class GetUserDayData extends Action
{
    /**
     * @param int $user_id
     * @param int $day_id
     * @return array
     */
    public function run(int $user_id, int $day_id)
    {
        $data = [];
        return $data;
    }
}
