<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Actions\Api\Adcbill\SubscribeAction;

class UserSubscribeAction extends ActionAbstract
{
    /**
     * @param array $params
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        $user = app('user')->auth($params);

        $params['platform'] = $user->platform;
        $params['bundle'] = $user->bundle_id;
        $params['lang'] = $user->lang;
        $params['country'] = $user->country;
        $request = [
            'data' => $params,
            'user_data' => $user->toArray(),
        ];
        $result = $this->manager->action(SubscribeAction::class)
            ->arguments([$request])->run();

        $transactionStatus = $result['transaction_status'] ?? null;
        if (!empty($transactionStatus && $transactionStatus == 'success')) {
            $user->subscribed = 'sub';
            $user->save();
        }
    }

}