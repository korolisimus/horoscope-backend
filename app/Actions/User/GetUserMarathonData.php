<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;

/**
 * Class GetUserMarathonData
 * @package App\Actions\User
 */
class GetUserMarathonData extends Action
{
    /**
     * @var array
     */
    private array $data = [];

    /**
     * @param int $marathon_id
     * @return array
     */
    public function run(int $marathon_id): array
    {
        if(!empty($this->data)){
            return $this->data;
        }
        $user = app('auth')->auth();
        $user_id = $user->id;
        $access = $this->call(GetUserMarathonAccess::class, [$marathon_id]);
        $this->data = [
            'subscribed' => $user->subscribed,
            'status' => $this->call(GetUserMarathonStatus::class, [$marathon_id, $access]),
            'access' => $access,
            'current_day' => $this->call(GetUserMarathonCurrentDay::class, [$user_id, $marathon_id, $access]),
            'workload' => $this->call(GetUserMarathonWorkload::class, [$user_id, $marathon_id]),
            'vote' => [
                'to_start' => 0,
                'to_end' => 0
            ]
        ];
        return $this->data;
    }
}
