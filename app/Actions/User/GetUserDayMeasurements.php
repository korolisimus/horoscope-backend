<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Tasks\User\GetUserSettingWithDefault;

/**
 * Class GetUserMarathonWorkload
 * @package App\Actions\User
 */
class GetUserDayMeasurements extends Action
{
    /**
     * @param int $user_id
     * @param int $entity_id
     * @return mixed
     */
    public function run(int $user_id, int $entity_id)
    {
        $settings = $this->call(GetUserSettingWithDefault::class, [
            ['user_id' => $user_id, 'type' => 'measurements', 'entity_id' => $entity_id]
        ], [], [
            ['first' => []],
            ['toArray' => []]
        ]);
        return $settings['settings'];
    }
}
