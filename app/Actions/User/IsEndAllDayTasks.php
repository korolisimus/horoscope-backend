<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Actions\UserEvent\GetUserEvents;
use App\Criterias\Core\EqualCriteria;
use App\Models\Marathon;
use App\Tasks\PublishedEntity\Get;

/**
 * Class IsEndAllDayTasks
 * @package App\Actions\User
 */
class IsEndAllDayTasks extends Action
{
    /**
     * @param array $params
     * @return bool
     */
    public function run(array $params = []): bool
    {
        $user_id = $params['user_id'];
        $marathon_id = $params['marathon_id'];
        $day_id = $params['day_id'];
        $methods = [
            ['addCriteria' => [new EqualCriteria('entity_type', Marathon::ATTRIBUTE_PREFIX)]],
            ['addCriteria' => [new EqualCriteria('entity_id', $marathon_id)]],
            ['withCasts' => [['object' => 'array']]]
        ];
        $user_events = app('user_events')->run($marathon_id);
        $entity = $this->call(Get::class, [], $methods, [
                ['first' => []],
                ['toArray' => []]
            ])['object'] ?? null;
        $all_tasks_finished = true;
        if(!empty($entity)){
            $ids = array_column($entity['days'],'id');
            $day_pos = array_search($day_id, $ids);
            if($day_pos !== false) {
                $day = $entity['days'][$day_pos];
                $tasks = $day['tasks'];
                foreach ($tasks as $task){
                    if(empty($user_events['tasks'][$task['id']]) || $user_events['tasks'][$task['id']] !== 'finished'){
                        $all_tasks_finished = false;
                        break;
                    }
                }
            }
        }
        return $all_tasks_finished;
    }
}