<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Actions\Notifications\PushAction;
use App\Actions\Translates\GetTranslatesByKeyNameAction;
use App\Criterias\Core\DateCriteria;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Criterias\Core\NotNullCriteria;
use App\Models\User;
use App\Tasks\GetTask;
use App\Tasks\MultipleUpdateTask;
use Carbon\Carbon;

class PushUsersPredictionsAction extends ActionAbstract
{
    /**
     * @param string $mode
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(string $mode = ''): array
    {
        if ($mode == 'all') {
            $coffeeUsers = $this->manager->task(GetTask::class)
                ->criteria(new NotNullCriteria('fcm'))
                ->criteria(new EqualCriteria('coffee_status', 'process'))
                ->model(User::class)->run()->toArray();
        } else {
            $coffeeUsers = $this->manager->task(GetTask::class)
                ->criteria(new NotNullCriteria('fcm'))
                ->criteria(new EqualCriteria('coffee_status', 'process'))
                ->criteria(new DateCriteria('coffee_process_date', Carbon::now()->subMinutes(20), '<'))
                ->model(User::class)->run()->toArray();
        }
        $ids = array_column($coffeeUsers, 'id');

        $this->manager->task(MultipleUpdateTask::class)
            ->model(User::class)
            ->criteria(new InCriteria('id', $ids))
            ->arguments([['coffee_status' => 'show']])->run();

        $coffeeIds = array_column($coffeeUsers, 'fcm');
        $result = [];

        //action
        $translates = $this->manager->action(GetTranslatesByKeyNameAction::class)
            ->arguments([['PUSH_HAND_TITLE', 'PUSH_HAND_BODY', 'PUSH_COFFEE_TITLE', 'PUSH_COFFEE_BODY'], 'ar'])
            ->run();

        if (!empty($coffeeIds)) {
            $result[] = $this->manager->action(PushAction::class)->arguments(
                [
                    'params' => [
                        'to' => $coffeeIds,
                        'title' => $translates['PUSH_COFFEE_TITLE'],
                        'body' => $translates['PUSH_COFFEE_BODY'],
                        'extra' => [
                            'data' => [
                                'type' => 'Coffee'
                            ]
                        ]
                    ]
                ])->run();
        }

        if ($mode == 'all') {
            $handUsers = $this->manager->task(GetTask::class)
                ->criteria(new NotNullCriteria('fcm'))
                ->criteria(new EqualCriteria('hand_status', 'process'))
                ->model(User::class)->run()->toArray();
        } else {
            $handUsers = $this->manager->task(GetTask::class)
                ->criteria(new NotNullCriteria('fcm'))
                ->criteria(new EqualCriteria('hand_status', 'process'))
                ->criteria(new DateCriteria('hand_process_date', Carbon::now()->subHours(20), '<'))
                ->model(User::class)->run()->toArray();

        }

        $ids = array_column($handUsers, 'id');

        $this->manager->task(MultipleUpdateTask::class)
            ->model(User::class)
            ->criteria(new InCriteria('id', $ids))
            ->arguments([['hand_status' => 'show']])->run();

        $handIds = array_column($handUsers, 'fcm');
        if (!empty($handIds)) {
            $result[] = $this->manager->action(PushAction::class)->arguments(
                [
                    'params' => [
                        'to' => $handIds,
                        'title' => $translates['PUSH_HAND_TITLE'],
                        'body' => $translates['PUSH_HAND_BODY'],
                        'extra' => [
                            'data' => [
                                'type' => 'Hand'
                            ]
                        ]
                    ]
                ])->run();
        }
        return $result;
    }
}