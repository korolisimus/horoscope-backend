<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Models\User;
use App\Tasks\GetByIdTask;

class GetAuthUserAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Exception
     */
    public function run(array $params = [])
    {
        $userId = app('auth')->auth($params)->entity;
        try {
            $data = $this->manager->task(GetByIdTask::class)
                ->arguments(['id' => $userId])
                ->model(User::class)
                ->run();
        } catch (\Throwable $e) {
            throw new \Exception('TOKEN_NOT_VALID', 401);
        }
        return $data;
    }
}