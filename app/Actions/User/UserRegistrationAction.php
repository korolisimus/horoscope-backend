<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;
use App\Actions\Api\Adcbill\RegistrationAction;
use App\Models\User;
use App\Tasks\CreateTask;
use App\Validators\App\UserValidator;

class UserRegistrationAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|null
     * @throws \Exception
     */
    public function run(array $params = [])
    {
        app(UserValidator::class)->with($params)->passesOrFail(UserValidator::RULE_REGISTRATION);
        $info = $this->manager->action(GetUserInfoAction::class)->run();
        $params['country'] = strtolower($info->country);
        $params['timezone'] = strtolower($info->timezone);
        $user = $this->manager->task(CreateTask::class)
            ->arguments(['params' => $params])
            ->model(User::class)
            ->run();
        if(empty($user->email)){
            $user->email = bin2hex(random_bytes(10)).'@'.$user->bundle_id;
            $result = $this->manager->action(RegistrationAction::class)
                ->arguments([$user->toArray()])->run();
            if(!empty($result['uuid'])){
                $user->uuid = $result['uuid'];
            }
            $user->save();
        }
        return $user;
    }
}