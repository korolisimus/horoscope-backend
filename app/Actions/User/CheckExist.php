<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Exceptions\AccountExist;
use App\Tasks\User\Get;

/**
 * Class CheckExist
 * @package App\Actions\User
 */
class CheckExist extends ActionAbstract
{
    public function run(array $params = [])
    {
        $email = $params['email'] ?? null;
        $bundle = $params['bundle_id'] ?? null;
        $user = $this->call(Get::class, [], [
            ['addCriteria' => [new EqualCriteria('email', $email)]],
            ['addCriteria' => [new EqualCriteria('bundle_id', $bundle)]]
        ], [
            ['first' => []]
        ]);
        if (!empty($user)) {
            throw new AccountExist('APP_EMAIL_REGISTERED', 402);
        }
    }
}
