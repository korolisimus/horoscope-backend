<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;

class IsSubscribeAction extends ActionAbstract
{
    /**
     * @param array $params
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        $user = app('user')->auth($params);
        $params['uuid'] = $user->uuid;
        $params['mode'] = 'unique_receipt';
        $params['bundle'] = $user->bundle_id;
        $params['platform'] = $user->platform;
        $result = $this->manager->action(\App\Actions\Api\Adcbill\IsSubscribeAction::class)
            ->arguments([$params])->run();
        $status = $result['status'] ?? null;

        if(!empty($status)){
            if($status == 'sub_current'){
                $user->subscribed = 'sub';
            }else{
                $user->subscribed = 'no-sub';

            }
            $user->save();
        }
    }
}