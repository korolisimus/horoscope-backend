<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;

/**
 * Class GetUserMarathonCurrentDay
 * @package App\Actions\User
 */
class GetUserMarathonCurrentDay extends Action
{
    /**
     * @param int $user
     * @param int $marathon_id
     * @param string $access
     * @return int
     */
    public function run(int $user, int $marathon_id, string $access)
    {
        $statuses = ['close' => 'first', 'open' => 'next', 'prize'=> 'current'];
        //get marathon status
        //get marathon access
        //get day status
        //get day access
        //get task status
        //get task access
        $day_period = 1 * 60;
        if ($access == 'close') {
            //return 0
        } elseif ($access == 'open') {
            //return next
        } elseif ($access == 'prize') {
            //return current get tournament current day
        }
        return 1;
    }
}
