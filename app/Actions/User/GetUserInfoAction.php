<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;


class GetUserInfoAction extends ActionAbstract
{
    /**
     * @return mixed
     */
    public function run()
    {
        $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
        return json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
    }
}
