<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;

/**
 * Class UserLogoutAction
 * @package App\Actions\User
 */
class UserLogoutAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return bool[]|false[]
     */
    public function run(array $params = []): array
    {
        try {
            $access = app('auth')->auth($params);
            $access->delete();
            return ['data' => true];
        } catch (\Throwable $e) {

        }
        return ['data' => false];
    }
}
