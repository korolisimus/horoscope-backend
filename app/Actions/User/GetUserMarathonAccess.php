<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Criterias\UserProduct\ActiveProduct;
use App\Tasks\User\UserProductGet;

/**
 * Class GetUserAccess
 * @package App\Actions\Marathon
 */
class GetUserMarathonAccess extends Action
{
    /**
     * @param int $marathon_id
     * @return string
     */
    public function run(int $marathon_id): string
    {
        $user = app('auth')->auth();
        $status = 'close';
        if ($user->subscribed == 'sub') {
            $status = 'open';
        }
        $product = $this->call(UserProductGet::class, [], [
            ['addCriteria' => [new ActiveProduct($user->id, $marathon_id)]]
        ], [
            ['first' => []]
        ]);
        if (!empty($product)) {
            $status = 'prize';
        }
        return $status;
    }
}
