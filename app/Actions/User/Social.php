<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Actions\ActionAbstract;


class Social extends ActionAbstract
{
    /**
     * @param array $params
     */
    public function run(array $params = [])
    {
        $social_id = $params['id'] ?? $params['googleId'] ?? null;
        $bundle = $params['bundle'] ?? null;
    }
}