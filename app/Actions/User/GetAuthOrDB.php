<?php declare(strict_types=1);

namespace App\Actions\User;

use App\Abstracts\Action;
use App\Actions\ActionAbstract;
use App\Tasks\User\Get;
use Throwable;

/**
 * Class GetAuthOrDB
 * @package App\Actions\User
 */
class GetAuthOrDB extends ActionAbstract
{
    /**
     * @param int $user_id
     * @return mixed|null
     */
    public function run(int $user_id)
    {
        try {
            $user = app('auth')->auth();
        } catch (Throwable $e) {
            $user = $this->call(Get::class, [], [
                ['id' => [$user_id]]
            ], [
                ['first' => []],
                ['not_empty' => []]
            ]);
        }
        return $user;
    }
}
