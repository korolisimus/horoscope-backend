<?php declare(strict_types=1);

namespace App\Actions\AccessToken;

use App\Actions\ActionAbstract;
use App\Models\AccessToken;
use App\Tasks\DeleteByIdTask;

class DeleteRegisteredAccessAction extends ActionAbstract
{
    /**
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run()
    {
        $access = app('auth')->auth();
        return $this->manager->task(DeleteByIdTask::class)
            ->arguments(['id' => $access->id])
            ->model(AccessToken::class)
            ->run();
    }
}