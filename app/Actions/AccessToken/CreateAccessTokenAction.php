<?php declare(strict_types=1);

namespace App\Actions\AccessToken;

use App\Actions\ActionAbstract;
use App\Models\AccessToken;
use App\Tasks\CreateTask;

class CreateAccessTokenAction extends ActionAbstract
{
    /**
     * @param $id
     * @param $type
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     * @throws \Exception
     */
    public function run($id, $type)
    {
        $params = [
            'token' => bin2hex(random_bytes(32)),
            'entity' => $id,
            'type' => $type
        ];

        return $this->manager->task(CreateTask::class)
            ->model(AccessToken::class)
            ->arguments(['params' => $params])
            ->run();
    }
}
