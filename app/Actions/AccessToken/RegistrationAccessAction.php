<?php declare(strict_types=1);

namespace App\Actions\AccessToken;

use App\Actions\ActionAbstract;
use App\Models\AccessToken;

class RegistrationAccessAction extends ActionAbstract
{
    public function run(AccessToken $access)
    {
        app('auth')->auth(['token' => $access->token]);
    }
}