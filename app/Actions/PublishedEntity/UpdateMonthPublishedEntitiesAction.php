<?php declare(strict_types=1);

namespace App\Actions\PublishedEntity;

use App\Actions\ActionAbstract;

class UpdateMonthPublishedEntitiesAction extends ActionAbstract
{
    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run()
    {
        array_map(function ($type) {
            $this->manager->action(SetNextActiveEntityAction::class)
                ->arguments([$type])->run();
        }, ['zodiac_month', 'haircut']);
    }
}
