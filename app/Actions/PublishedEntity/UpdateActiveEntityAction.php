<?php declare(strict_types=1);

namespace App\Actions\PublishedEntity;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Models\PublishedEntity;
use App\Tasks\GetFirstTask;

class UpdateActiveEntityAction extends ActionAbstract
{
    /**
     * @param string $type
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(string $type)
    {
        $find = $this->manager->task(GetFirstTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_type', $type))
            ->criteria(new EqualCriteria('status', 1))->run();
        if(empty($find)){
            $this->manager->action(SetNextActiveEntityAction::class)
                ->arguments([$type])->run();
        }
    }
}
