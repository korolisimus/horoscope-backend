<?php declare(strict_types=1);

namespace App\Actions\PublishedEntity;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\OrderByCriteria;
use App\Models\PublishedEntity;
use App\Tasks\GetFirstTask;
use App\Tasks\MultipleUpdateTask;
use App\Tasks\UpdateTask;

class SetNextActiveEntityAction extends ActionAbstract
{
    /**
     * @param string $type
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(string $type)
    {
        $entity = $this->manager->task(GetFirstTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_type', $type))
            ->criteria(new OrderByCriteria('use_date', 'asc'))
            ->run();

        $this->manager->task(MultipleUpdateTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_type', $type))
            ->arguments([['status' => 0]])
            ->run();

        if (!empty($entity)) {
            $this->manager->task(UpdateTask::class)
                ->arguments([$entity->id, ['status' => 1, 'use_date' => date('Y-m-d H:i:s', time())]])
                ->model(PublishedEntity::class)
                ->run();
        }

    }
}

