<?php declare(strict_types=1);

namespace App\Actions\Api\Adcbill;

use App\Actions\ActionAbstract;

class ActivityAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        $user = app('user')->auth($params);
        $request = [
            'action' => 'event',
            'type' => 'activity',
            'data' => $user->toArray()
        ];

        $url = "https://api.adcbill.com/api/events";

        return $this->manager->action(AdcbillRequestAction::class)
            ->arguments([$url, $request])->run();
    }
}
