<?php declare(strict_types=1);

namespace App\Actions\Api\Adcbill;

use App\Actions\ActionAbstract;

class RegistrationAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        $request = [
            'data' => $params,
            'action' => 'event',
            'type' => 'registration'
        ];

        $url = "https://api.adcbill.com/api/events";

        return $this->manager->action(AdcbillRequestAction::class)
            ->arguments([$url, $request])->run();
    }
}