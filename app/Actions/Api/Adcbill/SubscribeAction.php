<?php declare(strict_types=1);

namespace App\Actions\Api\Adcbill;


use App\Actions\ActionAbstract;

class SubscribeAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        $params['action'] = 'receipt';
        $params['type'] = 'receipt';

        $url = "https://api.adcbill.com/api/receipt";

        return $this->manager->action(AdcbillRequestAction::class)
            ->arguments([$url, $params])->run();
    }
}
