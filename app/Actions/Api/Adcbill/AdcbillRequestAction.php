<?php declare(strict_types=1);

namespace App\Actions\Api\Adcbill;

use App\Actions\ActionAbstract;

class AdcbillRequestAction extends ActionAbstract
{
    public function run(string $url, array $params = [])
    {
        $config = config('app');
        $token = $config['adcbill_token'] ?? null;

        if (empty($token)) {
            info("ERROR adcbill event: empty token");
            return null;
        }

        $params['token'] = $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }
}
