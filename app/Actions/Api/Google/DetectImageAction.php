<?php declare(strict_types=1);

namespace App\Actions\Api\Google;

use Google\Cloud\Vision\V1\Feature\Type;
use Google\Cloud\Vision\V1\Image;
use Google\Cloud\Vision\V1\ImageAnnotatorClient;

class DetectImageAction extends \App\Actions\ActionAbstract
{
    /**
     * @param string $base64
     * @return array
     * @throws \Google\ApiCore\ApiException
     */
    public function run(string $base64): array
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . storage_path('horoscope-vision-api.json'));
        $client = new ImageAnnotatorClient();
        $image = null;

        $image = new Image();
        $image->setContent(base64_decode($base64));

        $result = [];
        if (!empty($image)) {
            $annotation = $client->annotateImage(
                $image,
                [Type::LABEL_DETECTION]
            );
            $data = $annotation->getLabelAnnotations();
            if ($data->count()) {
                foreach ($data as $item) {
                    $result[$item->getDescription()] = $item->getScore();
                }
            }
        }

        return $result;
    }
}