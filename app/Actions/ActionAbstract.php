<?php declare(strict_types=1);

namespace App\Actions;

use App\Manager;
use App\Traits\RequiredMethodsTrait;

abstract class ActionAbstract
{
    use RequiredMethodsTrait;

    protected Manager $manager;

    public function __construct()
    {
        $this->manager = app('manager');
    }
}