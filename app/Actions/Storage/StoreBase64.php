<?php declare(strict_types=1);

namespace App\Actions\Storage;

class StoreBase64 extends ActionStorageAbstract
{
    /**
     * @param string $file
     * @return mixed
     */
    public function run(string $file)
    {
        return $this->driver->save_base64($file);
    }
}
