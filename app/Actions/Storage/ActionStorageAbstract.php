<?php declare(strict_types=1);

namespace App\Actions\Storage;

use App\Actions\ActionAbstract;
use App\Traits\LocalStorageTrait;

/**
 * Class ThumbnailVideo
 * @package App\Abstracts\Tasks\Storage
 */
abstract class ActionStorageAbstract extends ActionAbstract
{
    use LocalStorageTrait;
}
