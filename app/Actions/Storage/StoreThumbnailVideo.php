<?php declare(strict_types=1);

namespace App\Actions\Storage;


/**
 * Class ThumbnailVideo
 * @package App\Abstracts\Tasks\Storage
 */
class StoreThumbnailVideo extends ActionStorageAbstract
{
    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data = [])
    {
        return $this->driver->thumbnail_video($data);
    }
}
