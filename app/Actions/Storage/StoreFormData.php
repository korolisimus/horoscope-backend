<?php declare(strict_types=1);

namespace App\Actions\Storage;

use Illuminate\Http\UploadedFile;

/**
 * Class SaveFormData
 * @package App\Abstracts\Tasks\Storage
 */
class StoreFormData extends ActionStorageAbstract
{
    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return mixed
     */
    public function run(UploadedFile $file)
    {
        return $this->driver->save($file);
    }
}