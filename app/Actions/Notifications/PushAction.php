<?php declare(strict_types=1);

namespace App\Actions\Notifications;

use App\Actions\ActionAbstract;

/**
 * Class Push
 * @package App\Actions\Notifications
 */
class PushAction extends ActionAbstract
{
    public static array $template = [
        'to' => null,
        'title' => '',
        'body' => '',
        'extra' => [
            'url' => null
        ],
        'vibrate' => true,
        'sound' => true,
        'silent' => false,
        'bundle' => null
    ];

    /**
     * @param array $params
     * @return array|mixed|void
     */
    public function run(array $params = [])
    {
        $params = array_merge(self::$template, $params);
        if (empty($params['to'])) {
            return false;
        }
        if (!is_array($params['to'])) {
            $params['to'] = [$params['to']];
        }
        $fields = [
            'registration_ids' => $params['to'],
            'data' => $params['extra']
        ];
        if (empty($params['silent'])) {
            $fields = array_merge($fields, [
                'notification' => [
                    'title' => $params['title'],
                    'body' => $params['body'],
                    'icon' => '',
                    'image' => $params['extra']['image'] ?? '',
                    'click_action' => $params['extra']['url'] ?? '',
                    'sound' => $params['sound'] ? 'default' : null,
                    'vibrate' => $params['vibrate'] ?? null,
                ],
                'url' => $params['extra']['url'] ?? ''
            ]);
        }
        $headers = [
            'Authorization: key=' . env('FIREBASE_KEY'),
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true) ?? [];
    }
}
