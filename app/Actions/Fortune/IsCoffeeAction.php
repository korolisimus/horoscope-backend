<?php declare(strict_types=1);

namespace App\Actions\Fortune;

use App\Actions\ActionAbstract;

class IsCoffeeAction extends ActionAbstract
{
    public function run(array $data = []): bool
    {
        $result = array_intersect_key(
            $data,
            array_flip([
                "Coffee cup",
                "Food",
                "Drinkware",
                "Cup",
                "Ingredient"]));
        return count($result) > 0;
    }
}