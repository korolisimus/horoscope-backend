<?php declare(strict_types=1);

namespace App\Actions\Fortune;

use App\Actions\ActionAbstract;

class IsHandAction extends ActionAbstract
{
    public function run(array $data = []): bool
    {
        $result = array_intersect_key(
            $data,
            array_flip([
                'Hand',
                'Nail',
                'Wrist',
                'Thumb',
                'Finger',
                'Gesture']));

        return count($result) > 0;
    }
}