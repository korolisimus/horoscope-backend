<?php declare(strict_types=1);

namespace App\Actions\Fortune;


use App\Actions\ActionAbstract;
use App\Actions\Files\DeleteFilesAction;
use App\Criterias\Core\EqualCriteria;
use App\Models\File;
use App\Tasks\GetTask;


class DeleteUserFilesByTypeAction extends ActionAbstract
{
    /**
     * @param int $userId
     * @param string $type
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(int $userId, string $type)
    {
        $files = $this->manager->task(GetTask::class)
            ->criteria(new EqualCriteria('type', $type))
            ->criteria(new EqualCriteria('user_id', $userId))
            ->model(File::class)
            ->run();
        $this->manager->action(DeleteFilesAction::class)
            ->arguments(['files' => $files])->run();

    }
}
