<?php declare(strict_types=1);

namespace App\Actions\Fortune;

use App\Actions\ActionAbstract;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class GetUserCoffeeFilesAction
 * @package App\Actions\Fortune
 */
class GetUserCoffeeFilesAction extends ActionAbstract
{
    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \Exception
     */
    public function run(array $params = []): Collection
    {
        /** @var \App\Models\User $user */
        $user = app('user')->auth($params);
        return $user->coffeeFiles()->get();
    }
}
