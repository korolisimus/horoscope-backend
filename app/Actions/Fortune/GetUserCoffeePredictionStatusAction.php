<?php declare(strict_types=1);

namespace App\Actions\Fortune;

use App\Actions\ActionAbstract;
use App\Constants\FortunePredictionStatusConstant;

class GetUserCoffeePredictionStatusAction extends ActionAbstract
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run(): string
    {
        $user = app('user')->auth();
        $coffeeFiles = $user->coffeeFiles()->get();

        if ($user->coffee_status == FortunePredictionStatusConstant::SHOW && $coffeeFiles->count() >= 3) {
            $predictionStatus = FortunePredictionStatusConstant::SHOW;
        } else {
            if ($coffeeFiles->count() >= 3 && $user->coffee_status == FortunePredictionStatusConstant::PROCESS) {
                $predictionStatus = FortunePredictionStatusConstant::PROCESS;
            } else {
                $predictionStatus = FortunePredictionStatusConstant::HIDE;
                $user->coffee_status = FortunePredictionStatusConstant::HIDE;
                $user->save();
            }
        }

        return $predictionStatus;
    }
}
