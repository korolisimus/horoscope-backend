<?php declare(strict_types=1);

namespace App\Actions\Fortune;

use App\Actions\ActionAbstract;
use App\Constants\FortunePredictionStatusConstant;

class GetUserHandPredictionStatusAction extends ActionAbstract
{
    /**
     * @return string
     * @throws \Exception
     */
    public function run(): string
    {
        /** @var \App\Models\User $user */
        $user = app('user')->auth();
        $handFiles = $user->handFiles()->get();
        if ($user->hand_status == FortunePredictionStatusConstant::SHOW && $handFiles->count() > 0) {
            $predictionStatus = FortunePredictionStatusConstant::SHOW;
        } else {
            if ($handFiles->count() > 0 && $user->hand_status == FortunePredictionStatusConstant::PROCESS) {
                $predictionStatus = FortunePredictionStatusConstant::PROCESS;
            } else {
                $predictionStatus = FortunePredictionStatusConstant::HIDE;
                $user->hand_status = FortunePredictionStatusConstant::HIDE;
                $user->save();
            }
        }

        return $predictionStatus;
    }
}
