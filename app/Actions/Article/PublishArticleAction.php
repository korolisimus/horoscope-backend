<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Criterias\Core\EqualCriteria;
use App\Models\Article;
use App\Models\PublishedEntity;
use App\Tasks\DeleteTask;
use App\Tasks\InsertOrUpdateTask;

class PublishArticleAction extends ActionAbstract
{
    /**
     * @param \App\Models\Article $model
     * @throws \Exception
     */
    public function run(Article $model)
    {
        $publishEntity = $this->manager->action(MakePublishEntityAction::class)
            ->arguments(['id' => $model->id])->run();

        $type = $model->getEntityType();

        $this->manager->task(DeleteTask::class)
            ->criteria(new EqualCriteria('entity_id', $model->id))
            ->model(PublishedEntity::class)->run();
        $status = 0;
        if (in_array($type, ['mind', 'heart', 'destiny', 'life', 'coffee', 'compare'])) {
            $status = 1;
        }
        $params = [
            'entity_id' => $model->id,
            'entity_type' => $type,
            'data' => json_encode($publishEntity),
            'status' => $status
        ];
        $this->manager->task(InsertOrUpdateTask::class)
            ->model(PublishedEntity::class)
            ->arguments(['params' => [$params]])->run();
    }


}
