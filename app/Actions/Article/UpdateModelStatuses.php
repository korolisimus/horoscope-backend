<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Actions\Translates\GetTranslateStatusAction;
use App\Models\Article;

/**
 * Class UpdateModelStatuses
 * @package App\Actions\Article
 */
class UpdateModelStatuses extends ActionAbstract
{
    /**
     * @param \App\Models\Article $model
     * @throws \Exception
     */
    public function run(Article $model)
    {
        $bindKeys = $this->manager->action(GetBindKeyValuesAction::class)
            ->arguments(['model' => $model])->run();
        $model->translate_status = $this->manager->action(GetTranslateStatusAction::class)
            ->arguments(['bindKeys' => $bindKeys])->run();
        $model->publish_status = $this->manager->action(GetPublishStatusAction::class)
            ->arguments(['model' => $model])->run();
        $model->save();
    }
}
