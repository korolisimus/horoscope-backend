<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Models\Article;

class MakeNullObjectAction extends ActionAbstract
{
    public function run(string $type)
    {
        $modelName = Article::getRelationType($type);
        $model = new $modelName;
        $model->data = $model::generateData();
        return $model;
    }
}