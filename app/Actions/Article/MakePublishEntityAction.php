<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;

class MakePublishEntityAction extends ActionAbstract
{
    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function run(int $id): array
    {
        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => $id])->run();
        $keys = $model->keys()->with(['langs'])->get()->toArray();
        $info = array_combine(array_column($keys, 'key'), $keys);
        $data = $model->toArray();
        $this->process($data, $info);
        return array_intersect_key($data, array_flip([
            'name',
            'type',
            'text',
            'info',
            'data',
            'translate_info',
            'translates',
        ]));
    }

    public function process(&$data, $info)
    {
        if (is_array($data)) {
            if (!empty($data['translate_info'])) {
                $trans = [];
                foreach ($data['translate_info'] as $k => $i) {
                    try {
                        $trans[$k] = array_combine(
                            array_column($info[$i]['langs'], 'lang'),
                            array_column($info[$i]['langs'], 'value')
                        );
                    } catch (\Throwable $e) {
                        info($info);
                        info($i);
                        info($k);
                    }
                }
                $data['translates'] = $trans;
            } else {
                foreach ($data as &$item) {
                    $this->process($item, $info);
                }
            }
        }
    }
}
