<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Constants\PublishStatusConstant;
use App\Criterias\Core\EqualCriteria;
use App\Models\Article;
use App\Models\PublishedEntity;
use App\Tasks\GetFirstTask;

class GetPublishStatusAction extends ActionAbstract
{
    /**
     * @param \App\Models\Article $model
     * @return string
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(Article $model): string
    {
        $type = $model->getEntityType();

        $entity = $this->manager->task(GetFirstTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_id', $model->id))
            //->criteria(new EqualCriteria('entity_type', $type))
            ->run();

        if (empty($entity)) {
            return PublishStatusConstant::UNPUBLISHED;
        } else {
            if ($entity->entity_type != $type) {
                return PublishStatusConstant::NEED_UPDATE;
            }
        }

        $entityData = $entity->data;
        $publishData = $this->manager->action(MakePublishEntityAction::class)
            ->arguments(['id' => $model->id])->run();

        if ($entityData != $publishData) {
            return PublishStatusConstant::NEED_UPDATE;
        }
        return PublishStatusConstant::PUBLISHED;
    }
}
