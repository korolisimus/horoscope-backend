<?php declare(strict_types=1);


namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Models\ModelAbstract;

class GetBindKeyValuesAction extends ActionAbstract
{
    private array $bindKeys = [];

    public function run(ModelAbstract $model): array
    {
        $data = $model->toArray();
        $this->process($data);
        return $this->bindKeys;
    }

    private function process($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $items) {
                if ($key === 'translate_info' && is_array($items)) {
                    foreach ($items as $field => $translateKey) {
                        $this->bindKeys[$translateKey] = $data[$field];
                    }
                } else {
                    $this->process($items);
                }
            }
        }
    }
}
