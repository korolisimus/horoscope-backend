<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Models\ModelAbstract;
use App\Tasks\UpdateTask;

class UpdateModelValuesByTranslateKeyAction extends ActionAbstract
{
    /**
     * @param \App\Models\ModelAbstract $model
     * @param $keyUpdate
     * @param $value
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(ModelAbstract $model, $keyUpdate, $value)
    {
        $data = $model->toArray();
        $this->process($data, $keyUpdate, $value);
        $this->manager->task(UpdateTask::class)
            ->arguments(['id' => $data['id'], 'params' => $data])
            ->model(get_class($model))->run();
    }

    private function process(&$data, $keyUpdate, $value)
    {
        if (!is_array($data)) {
            return;
        }
        foreach ($data as $key => &$item) {
            if ($key === 'translate_info' && !empty($item)) {
                $index = array_search($keyUpdate, $item);
                if ($index !== false) {
                    $data[$index] = $value;
                }
            } else {
                if (is_array($item)) {
                    $this->process($item, $keyUpdate, $value);
                }
            }
        }
    }
}
