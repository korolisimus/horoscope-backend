<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Models\Article;
use App\Tasks\GetByIdTask;

class MakeModelByIdAction extends ActionAbstract
{
    /**
     * @param int $id
     * @return mixed|void|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(int $id)
    {
        $article = $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $id])
            ->model(Article::class)
            ->run();

        $modelName = Article::getRelationType($article->type);

        return $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $id])
            ->model($modelName)
            ->run();
    }
}