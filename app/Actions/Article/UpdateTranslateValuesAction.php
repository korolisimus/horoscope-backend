<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Actions\Translates\GetExistLangsAction;
use App\Criterias\Core\InCriteria;
use App\Models\ModelAbstract;
use App\Models\Translate;
use App\Models\TranslateKey;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;

class UpdateTranslateValuesAction extends ActionAbstract
{
    /**
     * @param \App\Models\ModelAbstract $model
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(ModelAbstract $model)
    {
        $bindKeys = $this->manager->action(GetBindKeyValuesAction::class)
            ->arguments(['model' => $model])->run();

        $existKeyObjects = $this->manager->task(GetTask::class)
            ->criteria(new InCriteria('key', array_keys($bindKeys)))
            ->method('with', ['langs'])
            ->model(TranslateKey::class)
            ->run()->toArray();

        $existKeysIds = array_combine(array_column($existKeyObjects, 'key'), array_column($existKeyObjects, 'id'));
        $existLangs = $this->manager->action(GetExistLangsAction::class)->run();
        $updateRu = [];
        $updateOther = [];
        foreach ($bindKeys as $key => $value) {
            if (!empty($existKeysIds[$key])) {
                $updateRu[] = [
                    'key_id' => $existKeysIds[$key],
                    'value' => $value,
                    'is_valid' => 1,
                    'lang' => 'ru'
                ];
                foreach ($existLangs as $lang) {
                    if ($lang['lang'] != 'ru') {
                        $updateOther[] = [
                            'key_id' => $existKeysIds[$key],
                            'lang' => $lang['lang']
                        ];
                    }
                }
            }
        }

        $this->manager->task(InsertOrUpdateTask::class)
            ->model(Translate::class)
            ->arguments(['params' => $updateRu])
            ->run();

        $this->manager->task(InsertOrUpdateTask::class)
            ->model(Translate::class)
            ->arguments(['params' => $updateOther])
            ->run();
    }

}