<?php declare(strict_types=1);

namespace App\Actions\Article;

use App\Actions\ActionAbstract;
use App\Actions\Translates\StoreKeysToTranslateAction;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\InCriteria;
use App\Models\Article;
use App\Models\Translate;
use App\Models\TranslateKey;
use App\Models\TranslateLang;
use App\Tasks\GetTask;
use App\Tasks\InsertOrUpdateTask;

class SendToTranslateArticleAction extends ActionAbstract
{
    /**
     * @param \App\Models\Article $model
     * @return bool
     * @throws \Exception
     */
    public function run(Article $model): bool
    {

        $bindKeys = $this->manager->action(GetBindKeyValuesAction::class)
            ->arguments(['model' => $model])->run();

        if (!empty($bindKeys)) {

            $keysArray = array_keys($bindKeys);
            $this->manager->action(StoreKeysToTranslateAction::class)
                ->arguments(['keys' => $keysArray])->run();

            $transactionKeys = $this->manager->task(GetTask::class)
                ->criteria(new InCriteria('key', $keysArray))
                ->model(TranslateKey::class)->run();

            $info = [];
            foreach ($transactionKeys as $translateKey) {
                $info[] = [
                    'id' => $translateKey->id,
                    'key' => $translateKey->key,
                    'value' => $bindKeys[$translateKey->key]
                ];
            }

            $ids = array_column($transactionKeys->toArray(), 'id');

            $model->keys()->sync($ids);

            $this->updateTranslates($info);

        }

        return true;
    }

    /**
     * @param array $info
     * @throws \Exception
     */
    private function updateTranslates(array $info = [])
    {

        $languages = $this->manager->task(GetTask::class)
            ->model(TranslateLang::class)
            //->criteria(new EqualCriteria('status', 1))
            ->run()->toArray();
        $updateRu = [];
        $updateOther = [];
        foreach ($info as $item) {
            foreach ($languages as $lang) {
                if ($lang['lang'] == 'ru') {
                    $updateRu[] = [
                        'value' => $item['value'],
                        'memory_value' => $item['value'],
                        'is_valid' => 1,
                        'lang' => 'ru',
                        'key_id' => $item['id']
                    ];
                } else {
                    $updateOther[] = [
                        'key_id' => $item['id'],
                        'lang' => $lang['lang'],
                        'is_valid' => 0
                    ];
                }

            }
        }

        if (!empty($updateRu)) {
            $this->manager->task(InsertOrUpdateTask::class)
                ->arguments(['params' => $updateRu])->model(Translate::class)->run();
        }

        if (!empty($updateOther)) {
            $this->manager->task(InsertOrUpdateTask::class)
                ->arguments(['params' => $updateOther])->model(Translate::class)->run();
        }

    }
}