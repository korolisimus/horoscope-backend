<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\HasTranslateFieldsTrait;

/**
 * Class Article
 * @package App\Models
 * @property int $id
 * @property array $data
 * @property string $type
 * @property string $translate_status
 * @property string $publish_status
 */
class Article extends ModelAbstract implements TranslateModelInterface
{
    use HasTranslateFieldsTrait;

    /**
     * @var array|string[]
     */
    protected static array $relationTypes = [
        'compare' => Compare::class,
        'daily' => Daily::class,
        'haircut' => Haircut::class,
        'prediction' => Prediction::class,
        'mind' => FortuneHand::class,
        'heart' => FortuneHand::class,
        'destiny' => FortuneHand::class,
        'life' => FortuneHand::class,
        'coffee' => Fortune::class,
        'astro' => Astro::class,
        'zodiac' => Zodiac::class,
        'zodiac_month' => Zodiac::class,
        'zodiac_daily' => Zodiac::class,
    ];
    /**
     * @var string
     */
    protected $table = 'articles';
    protected $attributes = [
        'name' => null,
        'type' => null,
        'text' => null,
        'info' => null,
        'data' => null,
        'period_type' => null,
        'translate_status' => null,
        'publish_status' => null
    ];

    /**
     * @var string[]
     */
    protected $fillable = [
        'id', 'name', 'type', 'text', 'info', 'data', 'period_type', 'translate_status', 'publish_status', 'translate_info', 'default_lang'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'period_type', 'updated_at'
    ];

    /**
     * @var array|\string[][][]
     */
    protected array $schema = [
        'type' => ["=" => ['type']],
        'search' => ['like' => ['name', 'text']]
    ];

    /**
     * @var string[]
     */
    protected $guarded = ['translate_status', 'publish_status', 'translate_info'];

    /**
     * @var string[]
     */
    protected $casts = [
        'data' => 'array',
        'translate_info' => 'array'
    ];

    public static function generateData(): array
    {
        return [];
    }

    /**
     * @param $type
     * @return mixed|string
     */
    public static function getRelationType($type): string
    {
        return self::$relationTypes[$type] ?? Article::class;
    }

    public function getEntityType(): string
    {
        return $this->type;
    }

    public function publishedEntity(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PublishedEntity::class,'entity_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function keys(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(TranslateKey::class, 'articles_translate_keys', 'article_id', 'key_id',);
    }
}
