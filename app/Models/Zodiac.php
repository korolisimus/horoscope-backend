<?php declare(strict_types=1);

namespace App\Models;


use App\Constants\ZodiacNamesConstant;

class Zodiac extends Article implements GenerateDataInterface, HasTypeInterface
{
    protected static array $translateFields = [
        [
            'type' => 'key',
            'path' => 'data[key].options[key].value',
        ]
    ];
    protected $attributes = [
        'name' => null,
        'type' => null,
        'text' => null,
        'info' => null,
        'data' => null,
        'period_type' => 'daily',
        'translate_status' => null,
        'publish_status' => null
    ];
    protected $hidden = [
        'text', 'updated_at'
    ];
    /**
     * @var array|\string[][][]
     */
    protected array $schema = [
        'period_type' => ["=" => ['period_type']],
        'type' => ["=" => ['type']],
        'search' => ['like' => ['name', 'text']]
    ];

    public static function generateData(): array
    {
        $zodiacs = ZodiacNamesConstant::ZODIAC_NAMES;
        $result = [];
        foreach ($zodiacs as $key => $name) {
            $result[] = [
                'key' => $key,
                'name' => $name,
                'options' => [
                    /*[
                        'key' => 'health',
                        'name' => 'Здоровье',
                        'value' => ''
                    ],
                    [
                        'key' => 'love',
                        'name' => 'Любовь',
                        'value' => ''
                    ],
                    [
                        'key' => 'career',
                        'name' => 'Карьера',
                        'value' => ''
                    ]*/
                    [
                        'key' => 'common',
                        'name' => 'Общий',
                        'value' => ''
                    ],
                    [
                        'key' => 'love',
                        'name' => 'Любовный',
                        'value' => ''
                    ],
                    [
                        'key' => 'business',
                        'name' => 'Бизнес',
                        'value' => ''
                    ],
                    [
                        'key' => 'common_percent',
                        'name' => 'Процент здоровье',
                        'value' => ''
                    ],
                    [
                        'key' => 'love_percent',
                        'name' => 'Процент любовь',
                        'value' => ''
                    ],
                    [
                        'key' => 'business_percent',
                        'name' => 'Процент карьера',
                        'value' => ''
                    ],
                ]
            ];
        }
        return $result;
    }

    public static function getType(): string
    {
        return 'zodiac';
    }

    public function getEntityType(): string
    {
        return $this->type . '_' . $this->period_type;
    }
}