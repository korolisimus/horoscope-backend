<?php declare(strict_types=1);

namespace App\Models;


class Astro extends Article implements HasTypeInterface
{

    protected static array $translateFields = [
        [
            'type' => 'key',
            'path' => 'text'
        ],
    ];

    public static function getType(): string
    {
        return 'astro';
    }
}