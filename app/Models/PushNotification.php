<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class PushNotification
 * @package App\Models
 */
class PushNotification extends ModelAbstract
{
    /**
     * @var string
     */
    protected $table = 'push_notifications';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name', 'text', 'date', 'geos', 'platforms', 'success', 'failure', 'status', 'updated_at', 'created_at'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'geos' => 'array',
        'platforms' => 'array',
    ];

}
