<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\HasModelRequestSchema;
use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

/**
 * Class ModelAbstract
 * @package App\Models
 * @method static insertOnDuplicateKey(array $params)
 */
abstract class ModelAbstract extends Model
{
    use InsertOnDuplicateKey;
    use HasModelRequestSchema;
}
