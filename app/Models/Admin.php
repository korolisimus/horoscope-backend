<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Admin
 * @package App\Models
 * @property int $id
 */
class Admin extends ModelAbstract
{
    public const SUPER_ADMIN = 0;
    public const ADMIN = 1;
    public const STAFF = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'password', 'role_id', 'updated_at', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'updated_at', 'created_at'
    ];

    /**
     * @return HasOne
     */
    public function avatar()
    {
        return $this->hasOne(File::class, 'id', 'avatar');
    }
}
