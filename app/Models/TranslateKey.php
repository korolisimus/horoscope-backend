<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class TranslateKey
 * @package App\Models
 */
class TranslateKey extends ModelAbstract
{
    /**
     * @var array
     */
    protected $fillable = ['key', 'type', 'static', 'updated_at', 'created_at'];

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'created_at'];

    /**
     * @return HasMany
     */
    public function langs(): HasMany
    {
        return $this->hasMany(Translate::class, 'key_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function langRu(): HasOne
    {
        return $this->hasOne(Translate::class, 'key_id', 'id')->where('lang', '=', 'ru');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'articles_translate_keys', 'key_id', 'article_id');
    }
}
