<?php declare(strict_types=1);

namespace App\Models;

class Haircut extends Article implements GenerateDataInterface, HasTypeInterface
{
    protected static array $translateFields = [
        [
            'type' => 'unique',
            'path' => 'data[key].value',
            'model' => Prediction::class,
            'modelField' => 'text'
        ]
    ];

    public static function generateData(): array
    {
        $result = [];
        for ($i = 1; $i < 32; $i++) {
            $result[] = [
                'name' => 'День ' . $i,
                'key' => 'day_' . $i,
                'value' => ''
            ];
        }
        return $result;
    }

    public static function getType(): string
    {
        return 'haircut';
    }
}
