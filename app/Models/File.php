<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class File
 * @package App\Models
 */
class File extends ModelAbstract
{
    protected $table = 'files';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'path', 'path_1920', 'path_1334', 'path_1080', 'path_768', 'hash', 'size', 'image_height', 'image_width', 'type', 'user_id', 'template_id', 'updated_at', 'created_at'
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function getPathList(): array
    {
        return ['path', 'path_1920', 'path_1334', 'path_1080', 'path_768'];
    }
}
