<?php declare(strict_types=1);

namespace App\Models;

class Prediction extends Article implements HasTypeInterface
{
    public static function getType(): string
    {
        return 'prediction';
    }
}