<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class Translate
 * @package App\Models
 */
class Translate extends ModelAbstract
{
    /**
     * @var array
     */
    protected $fillable = ['lang', 'key_id', 'value', 'is_valid', 'updated_at', 'created_at'];

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'created_at'];


    public function lang(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(TranslateLang::class, 'lang', 'lang');
    }
}
