<?php declare(strict_types=1);

namespace App\Models;

use App\Constants\ArticleTypesConstant;

/**
 * Class User
 * @package App\Models
 * @property string $name
 * @property int $avatar
 * @property string $password
 * @property string $social_id
 * @property string $fcm
 * @property string $lang
 * @property int $pin
 * @property string $country
 * @property string $bundle_id
 * @property string $device_id
 * @property string $target
 * @property string $subscribed
 * @property string $platform
 * @property string $auth_type
 * @property string $uuid
 * @property string $timezone
 * @property string $birthday
 * @property string $place_of_birth
 * @property string $relationship_status
 * @property string $gender
 * @property string $coffee_status
 * @property string $hand_status
 * @property string $city
 * @property string $longitude
 * @property string $latitude
 * @property string $profile_step
 */
class User extends ModelAbstract
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'email', 'name', 'avatar', 'password', 'social_id', 'fcm', 'lang', 'country', 'bundle_id', 'device_id',
        'target', 'platform', 'auth_type', 'uuid', 'timezone', 'birthday', 'place_of_birth', 'relationship_status',
        'gender', 'coffee_status', 'hand_status', 'prediction_settings' ,'city', 'longitude', 'latitude', 'profile_step',
        'is_push', 'coffee_process_date', 'hand_process_date'
    ];

    /**
     * @var string[]
     */
    protected $hidden = [
        'password', 'pin', 'updated_at', 'created_at'
    ];


    /**
     * @var string[]
     */
    protected $guarded = [
        'password', 'pin', 'subscribed'
    ];


    protected $casts = [
        'prediction_settings' => 'array'
    ];

    public function coffeeFiles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(File::class, 'user_id', 'id')
            ->where('type', '=', ArticleTypesConstant::FORTUNE_COFFEE)
            ->orderBy('template_id');
    }

    public function handFiles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(File::class, 'user_id', 'id')
            ->where('type', '=', 'hand')
            ->orderBy('template_id');
    }
}
