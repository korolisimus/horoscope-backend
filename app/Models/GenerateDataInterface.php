<?php declare(strict_types=1);

namespace App\Models;

interface GenerateDataInterface
{
    public static function generateData();
}