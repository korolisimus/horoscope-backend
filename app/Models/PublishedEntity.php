<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class PublishedEntity
 * @package App\Models
 * @property object $data
 */
class PublishedEntity extends ModelAbstract
{
    /**
     * @var string
     */
    protected $table = 'published_entities';

    /**
     * @var string[]
     */
    protected $fillable = ['entity_id', 'entity_type', 'data', 'status', 'use_date', 'updated_at', 'created_at'];

    /**
     * @var string[]
     */
    protected $hidden = [
        'updated_at'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    protected $attributes = [
        'data' => null
    ];
}
