<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class TranslateLang
 * @package App\Models
 */
class TranslateLang extends ModelAbstract
{
    /**
     * @var array
     */
    protected $fillable = ['lang', 'status', 'updated_at', 'created_at'];

    /**
     * @var array
     */
    protected $hidden = ['updated_at', 'created_at'];
}
