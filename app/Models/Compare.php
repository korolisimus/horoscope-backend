<?php declare(strict_types=1);

namespace App\Models;


use App\Constants\ZodiacNamesConstant;

class Compare extends Article implements GenerateDataInterface, HasTypeInterface
{
    protected static array $translateFields = [
        [
            'type' => 'key',
            'path' => 'data[key].options[key].value',
        ]
    ];

    public static function generateData(): array
    {
        $zodiacs = ZodiacNamesConstant::ZODIAC_NAMES;


        $pair = $zodiacs;

        $result = [];
        foreach ($zodiacs as $key => $name) {
            $current = key($pair);
            while ($key != $current) {
                next($pair);
                $current = key($pair);
            }
            while ($pair_key = key($pair)) {
                $pair_name = current($pair);
                $result[] = [
                    'name' => $name . "-" . $pair_name,
                    'key' => $key . "-" . $pair_key,
                    'options' => [
                        [
                            'name' => 'Процент совместимости',
                            'key' => 'percent',
                            'value' => '',
                        ],
                        [
                            'name' => 'Любовь',
                            'key' => 'love',
                            'value' => '',
                        ],
                        [
                            'name' => 'Семья и брак',
                            'key' => 'family',
                            'value' => '',
                        ],
                        [
                            'name' => 'Секс',
                            'key' => 'sex',
                            'value' => '',
                        ]
                    ]
                ];
                next($pair);
            }
            reset($pair);
        }
        return $result;
    }

    public static function getType(): string
    {
        return 'compare';
    }
}