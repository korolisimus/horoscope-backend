<?php declare(strict_types=1);

namespace App\Models;


use App\Constants\ZodiacNamesConstant;

class Daily extends Article implements GenerateDataInterface, HasTypeInterface
{
    protected static array $translateFields = [
        [
            'type' => 'key',
            'path' => 'info',
        ],
        [
            'type' => 'key',
            'path' => 'data[key].options[key].value',
        ]
    ];

    public static function generateData(): array
    {
        $zodiacs = ZodiacNamesConstant::ZODIAC_NAMES;
        $result = [];
        foreach ($zodiacs as $key => $name) {
            $result[] = [
                'key' => $key,
                'name' => $name,
                'options' => [
                    [
                        'key' => 'common',
                        'name' => 'Общий',
                        'value' => ''
                    ],
                    [
                        'key' => 'love',
                        'name' => 'Любовный',
                        'value' => ''
                    ],
                    [
                        'key' => 'business',
                        'name' => 'Бизнес',
                        'value' => ''
                    ],
                    [
                        'key' => 'health_percent',
                        'name' => 'Процент здоровье',
                        'value' => ''
                    ],
                    [
                        'key' => 'love_percent',
                        'name' => 'Процент любовь',
                        'value' => ''
                    ],
                    [
                        'key' => 'career_percent',
                        'name' => 'Процент карьера',
                        'value' => ''
                    ],
                ]
            ];
        }
        return $result;
    }

    public static function getType(): string
    {
        return 'daily';
    }
}