<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class AccessToken
 * @package App\Models
 * @property string $token
 */
class AccessToken extends ModelAbstract
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'entity', 'type', 'updated_at', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];
}
