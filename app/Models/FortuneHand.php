<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class Article
 * @package App\Models
 */
class FortuneHand extends Fortune
{
    /**
     * @return \string[][]
     */
    public static function generateHandData(): array
    {
        return [
            'like_of_mind' => [
                'key' => 'like_of_mind',
                'name' => 'Линия ума',
                'value' => ''
            ],
            'like_of_heart' => [
                'key' => 'like_of_heart',
                'name' => 'Линия сердца',
                'value' => ''
            ],
            'like_of_destiny' => [
                'key' => 'like_of_destiny',
                'name' => 'Линия судьбы',
                'value' => ''
            ],
            'like_of_life' => [
                'key' => 'like_of_life',
                'name' => 'Линия жизни',
                'value' => ''
            ]
        ];
    }
}
