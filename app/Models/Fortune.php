<?php declare(strict_types=1);

namespace App\Models;

/**
 * Class Article
 * @package App\Models
 */
class Fortune extends Article
{
    protected static array $translateFields = [
        [
            'type' => 'key',
            'path' => 'text'
        ],
    ];

    protected array $schema = [
        'type' => ["in" => ['type']],
        'search' => ['like' => ['name', 'text']]
    ];

    protected $guarded = [];
}
