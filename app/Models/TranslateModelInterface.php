<?php declare(strict_types=1);

namespace App\Models;

interface TranslateModelInterface
{
    public const TRANSLATE_TYPE_KEY = 'key';
    public const TRANSLATE_TYPE_UNIQUE = 'unique';
    public const TRANSLATE_TYPE_NESTED = 'nested';

    public static function getTranslateFields(): array;

}