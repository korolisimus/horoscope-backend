<?php declare(strict_types=1);

namespace App\Models;

interface HasTypeInterface
{
    public static function getType(): string;
}