<?php declare(strict_types=1);

namespace App\Traits;

trait RequiredMethodsTrait
{
    protected array $required_methods = [];

    public function required_methods(): array
    {
        return $this->required_methods;
    }
}