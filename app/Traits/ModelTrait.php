<?php declare(strict_types=1);

namespace App\Traits;

trait ModelTrait
{
    private static string $model_class;

    public static function setModel(string $model_class): string
    {
        static::$model_class = $model_class;
        return static::class;
    }

    public function model(): string
    {
        return static::$model_class;
    }
}