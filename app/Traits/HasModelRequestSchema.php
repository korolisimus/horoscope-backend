<?php declare(strict_types=1);

namespace App\Traits;

/**
 * Trait HasModelRequestSchema
 * @package App\Traits
 */
trait HasModelRequestSchema
{
    /**
     * @var array
     */
    protected array $schema = [];

    /**
     * @return array
     */
    public function getRequestSchema(): array
    {
        return $this->schema;
    }
}