<?php declare(strict_types=1);

namespace App\Traits;

use App\Drivers\Local;

trait LocalStorageTrait
{
    protected object $driver;

    /**
     * Upload constructor.
     */
    public function __construct()
    {
        $this->driver = app(Local::class);
    }
}