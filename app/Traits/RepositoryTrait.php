<?php declare(strict_types=1);

namespace App\Traits;

use App\Presenters\Presenter;
use App\Repositories\CommonRepository;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Trait RepositoryTrait
 * @package App\Traits
 */
trait RepositoryTrait
{
    use RequiredMethodsTrait;

    /**
     * @var \Prettus\Repository\Eloquent\BaseRepository
     */
    protected BaseRepository $repository;

    /**
     * @param string $repository
     */
    public function repository(string $repository)
    {
        $this->repository = app($repository);
    }

    public function getRepository(): BaseRepository
    {
        return $this->repository;
    }

    /**
     * @param string $model
     */
    public function model(string $model)
    {
        $this->repository = app(CommonRepository::setModel($model));
    }

    /**
     * @param $data
     * @param string $transformer
     * @return array|null
     */
    public function transform($data, string $transformer): ?array
    {
        try {
            return (new Presenter())->setTransformer($transformer)->present($data);
        } catch (\Throwable $e) {
        }
        return null;
    }

    public function addCriteria(CriteriaInterface $criteria)
    {
        try {
            $this->repository->pushCriteria($criteria);
        } catch (\Throwable $e) {
        }
    }

    /**
     * @param array $arguments
     */
    public function with(array $arguments = [])
    {
        $this->repository->with($arguments);
    }

    /**
     * @param array $arguments
     */
    public function withCount(array $arguments = [])
    {
        $this->repository->withCount($arguments);
    }
}
