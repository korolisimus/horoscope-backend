<?php declare(strict_types=1);

namespace App\Traits;

trait HasTranslateFieldsTrait
{
    protected static array $translateFields = [];

    public static function getTranslateFields(): array
    {
        return static::$translateFields;
    }
}
