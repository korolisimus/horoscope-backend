<?php declare(strict_types=1);

namespace App\Traits;

use App\Manager;

trait ManagerTrait
{
    public Manager $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }
}