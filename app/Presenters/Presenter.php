<?php declare(strict_types=1);

namespace App\Presenters;

use League\Fractal\TransformerAbstract;
use Prettus\Repository\Presenter\FractalPresenter;

class Presenter extends FractalPresenter
{
    private TransformerAbstract $transformer;

    public function getTransformer(): \League\Fractal\TransformerAbstract
    {
        return $this->transformer;
    }

    public function setTransformer(string $transformer): Presenter
    {
        $this->transformer = app($transformer);
        return $this;
    }
}