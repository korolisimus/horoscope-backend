<?php declare(strict_types=1);

namespace App\Events\Admin;

use App\Events\Event;

/**
 * Class UpdateKeyEvent
 * @package App\Events
 */
class UpdateKeyValueEvent extends Event
{
    public int $keyId;

    public function __construct(int $keyId)
    {
        $this->keyId = $keyId;
    }
}