<?php declare(strict_types=1);

namespace App\Events\Admin;

use App\Events\Event;

class DeleteAdminModelEvent extends Event
{
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}