<?php declare(strict_types=1);

namespace App\Http\Middleware;

use App\Actions\Api\Adcbill\ActivityAction;
use App\Actions\Api\Adcbill\RegistrationAction;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class User
 * @package App\Http\Middleware
 */
class User
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        //DB::connection()->enableQueryLog();

        $access = app('auth')->auth();
        if ($access->type !== 'user') {
            throw new Exception('TOKEN_NOT_VALID', 401);
        }
        /** @var \App\Manager $manager */
        $manager = app('manager');
        $user = app('user')->auth();

        if (empty($user->uuid)) {
            if (empty($user->email)) {
                $user->email = bin2hex(random_bytes(10)) . '@' . $user->bundle_id;
            }
            $result = $manager->action(RegistrationAction::class)
                ->arguments([$user->toArray()])->run();
            if (!empty($result['uuid'])) {
                $user->uuid = $result['uuid'];
            }
            $user->save();
        }
        $manager->action(ActivityAction::class)->run();
        $request->merge(['user_id' => $access->entity]);

        $result = $next($request);

        //$queries = DB::getQueryLog();
        //info($queries);
        return $result;
    }
}
