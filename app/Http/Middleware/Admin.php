<?php declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\Request;

/**
 * Class Admin
 * @package App\Http\Middleware
 */
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        $access = app('auth')->auth();
        if ($access->type !== 'admin') {
            throw new Exception('TOKEN_NOT_VALID', 401);
        }
        return $next($request);
    }
}
