<?php declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

/**
 * Class CheckRequest
 * @package App\Http\Middleware
 */
class CheckRequest
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $update = [];
        foreach ($request->all() as $key => $item) {
            if ($item === 'null') {
                $update[$key] = null;
            }
            if ($item === 'true') {
                $update[$key] = true;
            }
            if ($item === 'false') {
                $update[$key] = false;
            }
        }
        $request->merge($update);

        return $next($request);
    }
}
