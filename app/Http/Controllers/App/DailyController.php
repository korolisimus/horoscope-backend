<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Services\App\DailyService;
use Illuminate\Http\Request;

class DailyController extends ArticleController
{
    public function __construct(Request $request, DailyService $service)
    {
        parent::__construct($request, $service);
    }
}