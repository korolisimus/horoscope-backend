<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Services\App\FortuneCoffeeService;
use Illuminate\Http\Request;

class CoffeeController extends ArticleController
{
    public function __construct(Request $request, FortuneCoffeeService $service)
    {
        parent::__construct($request, $service);
    }
}