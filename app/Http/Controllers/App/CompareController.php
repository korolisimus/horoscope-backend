<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Services\App\CompareService;
use Illuminate\Http\Request;

class CompareController extends ArticleController
{
    public function __construct(Request $request, CompareService $service)
    {
        parent::__construct($request, $service);
    }
}