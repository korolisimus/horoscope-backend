<?php declare(strict_types=1);

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Services\App\ArticleService;
use Illuminate\Http\Request;

/**
 * Class ArticleController
 * @package App\Http\Controllers\App
 */
class ArticleController extends Controller
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected Request $request;

    /**
     * @var \App\Services\App\ArticleService
     */
    protected ArticleService $service;

    /**
     * ArticleController constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\App\ArticleService $service
     */
    public function __construct(Request $request, ArticleService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }
}
