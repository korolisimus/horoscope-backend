<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Http\Controllers\Controller;
use App\Services\App\FortuneHandService;
use Illuminate\Http\Request;

/**
 * Class FortuneHandController
 * @package App\Http\Controllers\App
 */
class FortuneHandController extends Controller
{
    private Request $request;
    private FortuneHandService $service;


    public function __construct(Request $request, FortuneHandService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function storeFiles()
    {
        $data = $this->service->storeFiles($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function refresh()
    {
        $data = $this->service->refresh($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }
}