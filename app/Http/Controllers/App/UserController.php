<?php declare(strict_types=1);

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\App
 */
class UserController extends Controller
{
    /**
     * @var \App\Services\App\UserService
     */
    private \App\Services\App\UserService $service;

    /**
     * User constructor.
     * @param \App\Services\App\UserService $service
     */
    public function __construct(\App\Services\App\UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function getUserOrCreate(Request $request)
    {
        $data = $this->service->getUserOrCreate($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function update(Request $request)
    {
        $data = $this->service->update($request->all());
        return response($data);
    }


    public function subscribeFail()
    {
        $data = ['data' => true];
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function isSub(Request $request)
    {
        $data = $this->service->isSub($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function subscribe(Request $request)
    {
        $data = $this->service->subscribe($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function install(Request $request)
    {
        $data = $this->service->install($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function billCallback(Request $request)
    {
        $data = $this->service->billCallback($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function processHandPrediction(Request $request)
    {
        $data = $this->service->processHandPrediction($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function processCoffeePrediction(Request $request)
    {
        $data = $this->service->processCoffeePrediction($request->all());
        return response($data);
    }
}
