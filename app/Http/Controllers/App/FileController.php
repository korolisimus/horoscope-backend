<?php declare(strict_types=1);

namespace App\Http\Controllers\App;

use App\Actions\Files\StoreBase64Action;
use App\Http\Controllers\Controller;
use App\Services\App\FileService;
use App\Traits\ManagerTrait;
use Illuminate\Http\Request;

class FileController extends Controller
{
    use ManagerTrait;

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Exception
     */
    public function upload(Request $request): array
    {
        $data = $this->manager->action(StoreBase64Action::class)
            ->arguments(['params' => $request->all()])->run();
        return ['data' => $data];
    }
}
