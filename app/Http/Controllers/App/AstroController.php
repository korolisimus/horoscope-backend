<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Services\App\AstroService;
use Illuminate\Http\Request;

class AstroController extends ArticleController
{
    /**
     * AstroController constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\App\AstroService $service
     */
    public function __construct(Request $request, AstroService $service)
    {
        parent::__construct($request, $service);
    }
}