<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Http\Controllers\Controller;
use App\Services\App\FortuneCoffeeService;
use Illuminate\Http\Request;

/**
 * Class FortuneCoffeeController
 * @package App\Http\Controllers\App
 */
class FortuneCoffeeController extends Controller
{
    private Request $request;
    private FortuneCoffeeService $service;

    /**
     * FortuneCoffeeController constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\App\FortuneCoffeeService $service
     */
    public function __construct(Request $request, FortuneCoffeeService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function storeFiles()
    {
        $data = $this->service->storeFiles($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function refresh()
    {
        $data = $this->service->refresh($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }
}