<?php declare(strict_types=1);

namespace App\Http\Controllers\App;


use App\Services\App\HaircutService;
use Illuminate\Http\Request;

class HaircutController extends ArticleController
{
    public function __construct(Request $request, HaircutService $service)
    {
        parent::__construct($request, $service);
    }
}