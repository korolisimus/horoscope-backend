<?php

namespace App\Http\Controllers;

use App\Actions\Notifications\Push;
use App\Actions\PublishedEntity\UpdateDailyPublishedEntitiesAction;
use App\Actions\PublishedEntity\UpdateMonthPublishedEntitiesAction;
use App\Actions\User\PushUsersPredictionsAction;
use App\Services\Utils\Base;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function fail()
    {
        return response()->json([
            'error' => ["message" => 'Route not found', "code" => 418]
        ], 418);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function languages()
    {
        $service = app(Base::class);
        $data = $service->languages();
        return response($data, 200);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function countries()
    {
        $service = app(Base::class);
        $data = $service->countries();
        return response($data, 200);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function translates(Request $request)
    {
        /** @var \App\Services\Utils\Base $service */
        $service = app(Base::class);
        $data = $service->translates($request->all());
        return response($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function pushDev(Request $request): array
    {
        /** @var \App\Manager $manager */
        $manager = app('manager');
        return ['data' => $manager->action(PushUsersPredictionsAction::class)->arguments(['all'])->run()];
    }

    /**
     * @return string
     */
    public function updateDaily(): string
    {
        app('manager')->action(UpdateDailyPublishedEntitiesAction::class)->run();
        return 'done';
    }

    /**
     * @return string
     */
    public function updateMonth(): string
    {
        app('manager')->action(UpdateMonthPublishedEntitiesAction::class)->run();
        return 'done';
    }
}
