<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\DailyService;
use Illuminate\Http\Request;

/**
 * Class CompareController
 * @package App\Http\Controllers\Admin
 */
class DailyController extends ArticleController
{
    public function __construct(Request $request, DailyService $service)
    {
        $request->merge(['type' => 'daily']);
        parent::__construct($request, $service);
    }
}
