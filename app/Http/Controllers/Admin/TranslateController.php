<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Actions\Files\StoreFormDataAction;
use App\Http\Controllers\Controller;
use App\Services\Admin\TranslateService;
use Illuminate\Http\Request;


/**
 * Class Translate
 * @package App\Http\Controllers\Admin
 */
class TranslateController extends Controller
{
    /**
     * @var \App\Services\Admin\TranslateService
     */
    private TranslateService $service;

    /**
     * @var \Illuminate\Http\Request
     */
    private Request $request;

    /**
     * TranslateController constructor.
     * @param \App\Services\Admin\TranslateService $service
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(TranslateService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function list()
    {
        $data = $this->service->list($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function languagesList()
    {
        $data = $this->service->listLangs();
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function storeKey()
    {
        $data = $this->service->storeKey($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function storeLang()
    {
        $data = $this->service->storeLang($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function updateKey()
    {
        $data = $this->service->updateKey($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function storeTranslate()
    {
        $data = $this->service->storeTranslate($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function deleteKey()
    {
        $data = $this->service->deleteKey($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function deleteLang()
    {
        $data = $this->service->deleteLang($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function deleteTranslate()
    {
        $data = $this->service->deleteTranslate($this->request->all());
        return response($data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $path = $this->service->export($this->request->all());
        return response()->download($path, 'translate.xlsx');
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function import()
    {
        /** @var \App\Manager $manager */
        $manager = app('manager');
        $ids = $manager->action(StoreFormDataAction::class)
            ->arguments(['params' => [$this->request->allFiles()]])->run();

        $data = $this->service->import(array_shift($ids), $this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function send()
    {
        $data = $this->service->send($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function approve()
    {
        $data = $this->service->approve($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function publish()
    {
        $data = $this->service->publish($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function unpublished()
    {
        $data = $this->service->unpublished($this->request->all());
        return response($data);
    }
}
