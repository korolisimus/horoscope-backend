<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\PublishedEntityService;
use Illuminate\Http\Request;

class PublishedEntityController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Admin\PublishedEntityService $service
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateStatus(Request $request, PublishedEntityService $service){
        $data = $service->updateStatus($request->all());
        return response($data);
    }
}