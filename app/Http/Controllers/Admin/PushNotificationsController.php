<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\PushNotificationsService;

/**
 * Class PushNotificationsController
 * @package App\Http\Controllers\Admin
 */
class PushNotificationsController extends Controller
{
    private \Illuminate\Http\Request $request;

    private PushNotificationsService $service;

    public function __construct(\Illuminate\Http\Request $request, PushNotificationsService $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function create()
    {
        $data = $this->service->create($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function update()
    {
        $data = $this->service->update($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function copy()
    {
        $data = $this->service->copy($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function send()
    {
        $data = $this->service->send($this->request->all());
        return response($data);
    }
}