<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\AstroService;
use Illuminate\Http\Request;

class AstroController extends ArticleController
{
    public function __construct(Request $request, AstroService $service)
    {
        $request->merge(['type' => 'astro']);
        parent::__construct($request, $service);
    }
}