<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\HaircutService;
use Illuminate\Http\Request;

class HaircutController extends ArticleController
{
    public function __construct(Request $request, HaircutService $service)
    {
        $request->merge(['type' => 'haircut']);
        parent::__construct($request, $service);
    }
}