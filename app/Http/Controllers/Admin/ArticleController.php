<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ServiceAbstract;
use Illuminate\Http\Request;

/**
 * Class ArticleController
 * @package App\Http\Controllers\Admin
 */
abstract class ArticleController extends Controller
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected Request $request;


    protected ServiceAbstract $service;

    /**
     * ArticleController constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Admin\ServiceAbstract $service
     */
    public function __construct(Request $request, ServiceAbstract $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function create()
    {
        $data = $this->service->create($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function update()
    {
        $data = $this->service->update($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }
}