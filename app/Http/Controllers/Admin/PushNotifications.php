<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

/**
 * Class PushNotifications
 * @package App\Http\Controllers\Admin
 */
class PushNotifications extends Controller
{
    /**
     * @var \Illuminate\Http\Request
     */
    private \Illuminate\Http\Request $request;

    /**
     * @var \App\Services\PushNotifications
     */
    private \App\Services\PushNotifications $service;

    /**
     * PushNotifications constructor.
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\PushNotifications $service
     */
    public function __construct(\Illuminate\Http\Request $request, \App\Services\PushNotifications $service)
    {
        $this->request = $request;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function create()
    {
        $data = $this->service->create($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update()
    {
        $data = $this->service->update($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function copy()
    {
        $data = $this->service->copy($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function send()
    {
        $data = $this->service->send($this->request->all());
        return response($data);
    }
}