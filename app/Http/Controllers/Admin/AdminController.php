<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\AdminService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Laravel\Lumen\Http\ResponseFactory;
use Prettus\Validator\Exceptions\ValidatorException;

class AdminController extends Controller
{
    private AdminService $service;

    private Request $request;

    public function __construct(AdminService $service, Request $request)
    {
        $this->service = $service;
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function login()
    {
        $data = $this->service->login($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function logout()
    {
        $data = $this->service->logout();
        return response($data);
    }

    /**
     * @return Response|ResponseFactory
     * @throws ValidatorException
     */
    public function register()
    {
        $data = $this->service->register($this->request->all());
        return response($data);
    }

    /**
     * @return Response|ResponseFactory
     * @throws ValidatorException
     */
    public function delete()
    {
        $data = $this->service->delete($this->request->all());
        return response($data);
    }

    /**
     * @return Response|ResponseFactory
     * @throws ValidatorException
     */
    public function update()
    {
        $data = $this->service->update($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function list()
    {
        $data = $this->service->list($this->request->all());
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function show()
    {
        $data = $this->service->show();
        return response($data);
    }

    /**
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws \Exception
     */
    public function get()
    {
        $data = $this->service->get($this->request->all());
        return response($data);
    }
}
