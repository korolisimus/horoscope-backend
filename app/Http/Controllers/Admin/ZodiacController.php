<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\ZodiacService;
use Illuminate\Http\Request;

/**
 * Class ZodiacController
 * @package App\Http\Controllers\Admin
 */
class ZodiacController extends ArticleController
{
    public function __construct(Request $request, ZodiacService $service)
    {
        $request->merge(['type' => 'zodiac']);
        parent::__construct($request, $service);
    }
}
