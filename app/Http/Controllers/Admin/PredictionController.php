<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\ArticleService;
use Illuminate\Http\Request;

/**
 * Class PredictionController
 * @package App\Http\Controllers\Admin
 */
class PredictionController extends ArticleController
{
    public function __construct(Request $request, ArticleService $service)
    {
        $request->merge(['type' => 'prediction']);
        parent::__construct($request, $service);
    }
}
