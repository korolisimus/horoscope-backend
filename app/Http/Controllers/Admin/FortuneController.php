<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\FortuneService;
use Illuminate\Http\Request;

class FortuneController extends ArticleController
{
    public function __construct(Request $request, FortuneService $service)
    {
        parent::__construct($request, $service);
    }

    public function get()
    {
        $type = $this->request->get('type');
        if (empty($type)) {
            $type = ['hand', 'coffee', 'mind', 'heart', 'destiny', 'life'];
        } else {
            $type = [$type];
        }
        $this->request->merge(['type' => $type]);
        return parent::get();
    }
}