<?php declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Services\Admin\CompareService;
use Illuminate\Http\Request;

/**
 * Class CompareController
 * @package App\Http\Controllers\Admin
 */
class CompareController extends ArticleController
{
    public function __construct(Request $request, CompareService $service)
    {
        $request->merge(['type' => 'compare']);
        parent::__construct($request, $service);
    }
}
