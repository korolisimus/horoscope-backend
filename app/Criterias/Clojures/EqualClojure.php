<?php declare(strict_types=1);

namespace App\Criterias\Clojures;

class EqualClojure
{
    /**
     * @param $field
     * @param $value
     * @return \Closure
     */
    public static function make($field, $value): \Closure
    {
        return function ($q) use ($field, $value) {
            $q->where($field, '=', $value);
        };
    }
}