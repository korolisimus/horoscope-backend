<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LikeCriteria
 * @package App\Criterias\Core
 */
class LikeCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $value;

    /**
     * @var string
     */
    private string $separator;

    /**
     * @var string
     */
    private string $wildcard;

    /**
     * LikeCriteria constructor.
     * @param string $field
     * @param string $value
     * @param string $separator
     * @param string $wildcard
     */
    public function __construct(string $field, string $value, string $separator = ',', string $wildcard = '*')
    {
        $this->field = $field;
        $this->value = $value;
        $this->separator = $separator;
        $this->wildcard = $wildcard;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $values = explode($this->separator, $this->value);
            $query->where($this->field, 'LIKE', str_replace($this->wildcard, '%', array_shift($values)));
            foreach ($values as $value) {
                $query->orWhere($this->field, 'LIKE', str_replace($this->wildcard, '%', $value));
            }
        });
    }
}