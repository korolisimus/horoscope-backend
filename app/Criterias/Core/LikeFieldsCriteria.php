<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LikeFieldsCriteria
 * @package App\Criterias\Core
 */
class LikeFieldsCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private array $fields;

    /**
     * @var string
     */
    private string $value;

    /**
     * LikeFieldsCriteria constructor.
     * @param array $fields
     * @param string $value
     */
    public function __construct(array $fields, string $value)
    {
        $this->fields = $fields;
        $this->value = $value;

    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $query->where(array_shift($this->fields), 'LIKE', "%" . $this->value . "%");
            foreach ($this->fields as $field) {
                $query->orWhere($field, 'LIKE', "%" . $this->value . "%");
            }
        });
    }
}