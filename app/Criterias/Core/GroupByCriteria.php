<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GroupByCriteria
 * @package App\Criterias\Core
 */
class GroupByCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $select;

    /**
     * GroupByCriteria constructor.
     * @param $field
     * @param string $select
     */
    public function __construct($field, string $select = '*')
    {
        $this->field = $field;
        $this->select = $select;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->groupBy($this->field);
    }
}
