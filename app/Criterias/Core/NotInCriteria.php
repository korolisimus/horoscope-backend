<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NotInCriteria
 * @package App\Criterias\Core
 */
class NotInCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var array
     */
    private array $in;

    /**
     * InCriteria constructor.
     * @param string $field
     * @param array $in
     */
    public function __construct(string $field, array $in)
    {
        $this->field = $field;
        $this->in = $in;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNotIn($this->field, $this->in);
    }
}
