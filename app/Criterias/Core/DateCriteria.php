<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class DateCriteria
 * @package App\Criterias\Core
 */
class DateCriteria implements CriteriaInterface
{
    /**
     * @var \Carbon\Carbon
     */
    private Carbon $date;

    /**
     * @var string
     */
    private string $operator;

    /**
     * @var string
     */
    private string $field;

    /**
     * ThisBetweenDatesCriteria constructor.
     * @param string $field
     * @param \Carbon\Carbon $date
     * @param string $operator
     */
    public function __construct(string $field, Carbon $date, string $operator)
    {
        $this->field = $field;
        $this->date = $date;
        $this->operator = $operator;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, $this->operator, $this->date->format('Y-m-d H:i:s'));
    }
}
