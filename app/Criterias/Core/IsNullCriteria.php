<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class IsNullCriteria
 * @package App\Criterias\Core
 */
class IsNullCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * IsNullCriteria constructor.
     * @param string $field
     */
    public function __construct(string $field)
    {
        $this->field = $field;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereNull($this->field);
    }
}
