<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class HasLikeAttribute
 * @package App\Criterias\Core
 */
class HasLikeAttribute implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $search;

    /**
     * HasLikeAttribute constructor.
     * @param string $search
     */
    public function __construct(string $search)
    {
        $this->search = $search;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('attributes.keyId.langs_ru', function ($query) {
            $query->where('value', "like", "%$this->search%");
        });
    }
}
