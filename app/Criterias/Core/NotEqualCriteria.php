<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NotEqualCriteria
 * @package App\Criterias\Core
 */
class NotEqualCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var mixed
     */
    private $value;

    /**
     * NotEqualCriteria constructor.
     * @param string $field
     * @param $value
     */
    public function __construct(string $field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, '!=', $this->value);
    }
}
