<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EqualCriteria
 * @package App\Criterias\Core
 */
class EqualCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var mixed
     */
    private $value;

    /**
     * ThisEqualThatCriteria constructor.
     * @param string $field
     * @param mixed $value
     */
    public function __construct(string $field, $value)
    {
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, $this->value);
    }
}
