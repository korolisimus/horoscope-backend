<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderByRawCriteria implements CriteriaInterface
{
    private string $raw;

    public function __construct(string $raw){
        $this->raw = $raw;
    }

    public function apply($model, RepositoryInterface $repository){
        return $model->orderByRaw($this->raw);
    }
}