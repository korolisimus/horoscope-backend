<?php declare(strict_types=1);

namespace App\Criterias\Core;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LikeBeginCriteria
 * @package App\Criterias\Core
 */
class LikeBeginCriteria implements CriteriaInterface
{
    private string $field;
    private string $search;

    public function __construct(string $field, string $search)
    {
        $this->field = $field;
        $this->search = $search;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where($this->field, 'LIKE', $this->search . "%");
    }
}
