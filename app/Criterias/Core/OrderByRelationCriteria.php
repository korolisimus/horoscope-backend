<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrderByRelationCriteria
 * @package App\Criterias\Core
 */
class OrderByRelationCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $order;

    /**
     * OrderByRelationCriteria constructor.
     * @param $order
     */
    public function __construct($order)
    {
        $relations = [
            'ask' => 'asc',
            'desc' => 'desc'
        ];
        $this->order = $relations[$order] ?? 'asc';
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy('sort_attributes.keyId.langs_ru.value', $this->order);
    }
}
