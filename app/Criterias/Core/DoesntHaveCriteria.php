<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class DoesntHaveCriteria implements CriteriaInterface
{
    private string $relation;

    public function __construct(string $relation)
    {
        $this->relation = $relation;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereDoesntHave($this->relation);
    }
}