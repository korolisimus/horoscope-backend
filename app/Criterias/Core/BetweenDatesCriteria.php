<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class BetweenDatesCriteria
 * @package App\Criterias\Core
 */
class BetweenDatesCriteria implements CriteriaInterface
{
    /**
     * @var \Carbon\Carbon
     */
    private Carbon $start;

    /**
     * @var \Carbon\Carbon
     */
    private Carbon $end;

    /**
     * @var string
     */
    private string $field;

    /**
     * ThisBetweenDatesCriteria constructor.
     * @param string $field
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     */
    public function __construct(string $field, Carbon $start, Carbon $end)
    {
        $this->start = $start;
        $this->end = $end;
        $this->field = $field;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereBetween($this->field, [$this->start->toDateString(), $this->end->toDateString()]);
    }
}
