<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Illuminate\Support\Str;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrderByCriteria
 * @package App\Criterias\Core
 */
class OrderByCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $sortOrder;

    /**
     * OrderByCriteria constructor.
     * @param string $field
     * @param string $sortOrder
     */
    public function __construct(string $field, string $sortOrder = 'asc')
    {
        $this->field = $field;

        $sortOrder = Str::lower($sortOrder);
        $availableDirections = [
            'asc',
            'desc',
        ];

        if (array_search($sortOrder, $availableDirections) === false) {
            $sortOrder = 'asc';
        }

        $this->sortOrder = $sortOrder;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->orderBy($this->field, $this->sortOrder);
    }
}
