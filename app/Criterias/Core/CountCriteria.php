<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CountCriteria
 * @package App\Criterias\Core
 */
class CountCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $field;

    /**
     * ThisFieldCriteria constructor.
     *
     * @param $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return \Illuminate\Database\Query\Builder|mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return DB::table($model->getModel()->getTable())->select($this->field, DB::raw('count(' . $this->field . ') as total_count'))->groupBy($this->field);
    }
}
