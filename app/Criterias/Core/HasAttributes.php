<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class HasLikeAttribute
 * @package App\Criterias\Core
 */
class HasAttributes implements CriteriaInterface
{
    /**
     * @var array
     */
    private array $ids;

    /**
     * @var array
     */
    private array $names;

    /**
     * HasAttributes constructor.
     * @param array $names
     * @param array $ids
     */
    public function __construct(array $names, array $ids)
    {
        $this->ids = $ids;
        $this->names = $names;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('attributes', function ($query) {
            $query->whereIn('attribute_name', $this->names)->whereIn('key_id', $this->ids);
        });
    }
}
