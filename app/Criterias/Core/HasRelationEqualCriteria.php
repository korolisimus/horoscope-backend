<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class HasLikeAttribute
 * @package App\Criterias\Core
 */
class HasRelationEqualCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $relation;

    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $value;

    /**
     * HasRelationEqualCriteria constructor.
     * @param string $relation
     * @param string $field
     * @param string $value
     */
    public function __construct(string $relation, string $field, string $value)
    {
        $this->relation = $relation;
        $this->field = $field;
        $this->value = $value;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas($this->relation, function ($query) {
            $query->where($this->field, '=', $this->field);
        });
    }
}
