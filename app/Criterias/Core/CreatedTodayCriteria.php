<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;


/**
 * Class CreatedTodayCriteria
 * @package App\Criterias\Core
 */
class CreatedTodayCriteria implements CriteriaInterface
{
    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('created_at', '>=', Carbon::today()->toDateString());
    }
}
