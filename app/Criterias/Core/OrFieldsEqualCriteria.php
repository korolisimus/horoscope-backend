<?php declare(strict_types=1);

namespace App\Criterias\Core;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrFieldsEqualCriteria
 * @package App\Criterias\Core
 */
class OrFieldsEqualCriteria implements CriteriaInterface
{
    private array $data;

    /**
     * OrFieldsEqualCriteria constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $query->where(key($this->data), '=', current($this->data));
            next($this->data);
            while ($field = key($this->data)) {
                $value = current($this->data);
                $query->orWhere($field, '=', "$value");
                next($this->data);
            }
        });
    }
}
