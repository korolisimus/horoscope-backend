<?php declare(strict_types=1);

namespace App\Criterias;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class RequestCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private array $params;

    /**
     * RequestCriteria constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $schema = $model->getModel()->getRequestSchema();

        if (empty($schema)) {
            return $model;
        }

        foreach ($schema as $field => $info) {
            $operator = key($info);
            $fields = current($info);
            $value = $this->params[$field] ?? null;
            if ($operator == 'like') {
                $value = "%" . $value . "%";
            }
            if (empty($value)) continue;
            if ($operator == 'in') {
                $model = $model->where(function ($model) use ($fields, $value) {
                    $field = array_shift($fields);
                    $model = $model->whereIn($field, $value);
                    foreach ($fields as $field) {
                        $model = $model->orWhereIn($field, $value);
                    }
                });
            } else {
                $model = $model->where(function ($model) use ($fields, $operator, $value) {
                    $field = array_shift($fields);
                    $model = $model->where($field, $operator, $value);
                    foreach ($fields as $field) {
                        $model = $model->orWhere($field, $operator, $value);
                    }
                });
            }

        }

        return $model;
    }
}