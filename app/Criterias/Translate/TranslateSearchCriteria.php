<?php declare(strict_types=1);

namespace App\Criterias\Translate;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class TranslateSearchCriteria
 * @package App\Criterias\Translate
 */
class TranslateSearchCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    private string $search;

    /**
     * Like constructor.
     * @param string $search
     */
    public function __construct(string $search)
    {
        $this->search = $search;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('langs', function ($query) {
            $query->where('value', 'like', "%$this->search%");
        })->orWhere('key', 'like', "%$this->search%");
    }
}
