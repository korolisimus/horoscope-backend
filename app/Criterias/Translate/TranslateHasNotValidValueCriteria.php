<?php declare(strict_types=1);

namespace App\Criterias\Translate;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class TranslateHasNotValidValueCriteria
 * @package App\Criterias\Translate
 */
class TranslateHasNotValidValueCriteria implements CriteriaInterface
{
    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('langs', function ($query) {
            $query->whereNull('value')->orWhere('value', '=', '');
        });
    }
}
