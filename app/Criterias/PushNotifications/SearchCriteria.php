<?php declare(strict_types=1);

namespace App\Criterias\PushNotifications;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchFixtureCriteria
 * @package App\Criteria
 */
class SearchCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private array $params;


    public function __construct(array $params = [])
    {
        foreach ($params as $key => $param) {
            if ($param == 'null') {
                $params[$key] = null;
            } else if ($param == 'false') {
                $params[$key] = false;
            } else if ($param == 'true') {
                $params[$key] = true;
            }
        }
        $this->params = $params;
    }

    /**
     * @param $model
     * @param \Prettus\Repository\Contracts\RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where(function ($query) {
            $search = $this->params['search'] ?? null;
            if (!empty($search)) {
                $query->where('name', 'like', "%$search%");
            }
            $status = $this->params['status'] ?? null;
            if (!is_null($status)) {
                $query->where('status', '=', $status);
            }
            foreach (['platforms', 'geos', 'place'] as $item) {
                $data = $this->params[$item] ?? null;
                if (!empty($data)) {
                    if (!is_array($data)) {
                        $data = explode(",", $data);
                    }
                    $data = '"' . implode('","', $data) . '"';
                    $query->whereRaw("JSON_CONTAINS($item, '[$data]') = 1");
                }
            }
        });
    }
}