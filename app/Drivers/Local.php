<?php declare(strict_types=1);

namespace App\Drivers;

use Gumlet\ImageResize;
use Illuminate\Http\UploadedFile;
use Throwable;


class Local
{
    public const STORAGE_PATH = 'app/files';

    public function save(UploadedFile $file): ?array
    {
        try {
            $size = $file->getSize();
            $type = $file->getMimeType();
            //$ext = $file->extension();

            if (preg_match('/video\/*/', $type)) {
                $type = 'video';
            } else if (preg_match('/image\/*/', $type)) {
                $type = 'image';
            } else {
                $type = 'file';
            }

            $filename = $file->getClientOriginalName();

            $find = [" ", "'", '"'];
            $remove = [",", ":"];
            $filename = str_replace($find, '_', $filename);
            $filename = str_replace($remove, '', $filename);
            $upload_filename = bin2hex(random_bytes(16)) . $filename;
            $upload_filename = transliterator_transliterate('Russian-Latin/BGN', $upload_filename);

            $file->move(storage_path(self::STORAGE_PATH), $upload_filename);
            $hash = md5_file(storage_path(self::STORAGE_PATH) . "/" . $upload_filename);
            $name = explode(".", $filename)[0];
            $parts = explode('.', $upload_filename);
            $upload_name = $parts[0];
            $ext = $parts[count($parts) - 1];
            $result = ['path' => 'files/' . $upload_filename, 'name' => $name, 'size' => $size, 'hash' => $hash, 'type' => $type, 'ext' => $ext];
            if ($type == 'image') {
                $result = array_merge($result, [
                    //'path_1920' => $this->resize_image_width(1920, $upload_name, $ext),
                    //'path_1334' => $this->resize_image_width(1334, $upload_name, $ext),
                    //'path_1080' => $this->resize_image_width(1080, $upload_name, $ext),
                    'path_768' => $this->resize_image_width(768, $upload_name, $ext),
                ]);
            }
            return $result;
        } catch (Throwable $e) {
            info($e->getMessage());
            info($e->getFile());
            info($e->getLine());
        }
        return null;
    }

    /**
     * @param $width
     * @param $name
     * @param $ext
     * @return string
     * @throws \Gumlet\ImageResizeException
     */
    public function resize_image_width($width, $name, $ext): string
    {
        $path = storage_path("app/files/{$name}.{$ext}");
        $image = new ImageResize($path);
        $image_width = $image->getSourceWidth();
        if ($image_width <= $width) {
            return "files/{$name}.{$ext}";
        }
        $image->resizeToWidth($width);
        $new_path = "files/{$name}_{$width}.$ext";
        $image->save(storage_path("app/$new_path"));
        return $new_path;
    }

    public function save_base64(string $file): ?array
    {
        $ext = $this->getExt($file);
        $data = substr($file, strpos($file, ',') + 1);
        $data = base64_decode($data);
        $hash = md5($data);
        $uuid = uniqid('', true);
        $name = $uuid . "." . $ext;
        $path = storage_path('app/files') . "/$name";
        $result = null;
        try {
            file_put_contents($path, $data);
            $size = getimagesize($path);

            $result = [
                'path' => 'files/' . $name,
                //'path_1920' => $this->resize_image_width(1920, $uuid, $ext),
                //'path_1334' => $this->resize_image_width(1334, $uuid, $ext),
                //'path_1080' => $this->resize_image_width(1080, $uuid, $ext),
                'path_768' => $this->resize_image_width(768, $uuid, $ext),
                'name' => $name,
                'size' => filesize($path),
                'hash' => $hash,
                'image_height' => $size[1] ?? null,
                'image_width' => $size[0] ?? null,
                'type' => 'image',
                'ext' => $ext
            ];
        } catch (Throwable $e) {
            info($e->getMessage());
            info($e->getFile());
            info($e->getLine());
        }
        return $result;
    }

    public function getExt($uri): string
    {
        $img = explode(',', $uri);
        $img = explode(';', $img[0]);
        $img = explode('/', $img[0]);
        return $img[1];
    }

    public function delete($file)
    {
        unlink(storage_path('app') . "/" . $file->path);
    }

    public function store_data($type, $data)
    {
        $path = storage_path("app/info/$type");
        $item = $this->get_data($type);
        if (!is_null($item)) {
            $data = array_merge($item, $data);
        }
        file_put_contents($path, json_encode($data));
    }

    public function get_data($type): ?array
    {
        $path = storage_path("app/info/$type");
        try {
            $data = json_decode(file_get_contents($path), true) ?? null;
        } catch (Throwable $e) {
            $data = null;
        }

        return $data;
    }

    public function delete_data($type)
    {
        $path = storage_path("app/info/$type");
        unlink($path);
    }

    public function thumbnail_video(array $data = []): array
    {
        try {
            $path = $data['path'];
            $ext = 'png';
            $thumbnail_name = $data['hash'] . $data['name'] . "_thumbnail";
            $thumbnail_path = 'files/' . $thumbnail_name . ".$ext";
            $movie = storage_path("app/$path");
            $thumbnail = storage_path("app/$thumbnail_path");
            $ffmpeg = \FFMpeg\FFMpeg::create([
                'ffmpeg.binaries' => '/usr/bin/ffmpeg',
                'ffprobe.binaries' => '/usr/bin/ffprobe'
            ]);
            $video = $ffmpeg->open($movie);
            $frame = $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(1));
            $frame->save($thumbnail);
            $size = getimagesize($thumbnail);
            $hash = md5_file($thumbnail);
            return [
                'path' => $thumbnail_path,
                'path_1920' => $this->resize_image_width(1920, $thumbnail_name, $ext),
                'path_1334' => $this->resize_image_width(1334, $thumbnail_name, $ext),
                'path_1080' => $this->resize_image_width(1080, $thumbnail_name, $ext),
                'path_768' => $this->resize_image_width(768, $thumbnail_name, $ext),
                'name' => $data['name'] . "_thumbnail",
                'size' => filesize($thumbnail),
                'hash' => $hash,
                'image_height' => $size[1] ?? null,
                'image_width' => $size[0] ?? null,
                'type' => 'image',
                'ext' => $ext
            ];
        } catch (Throwable $e) {
            info($e->getMessage());
        }
        return [];
    }
}
