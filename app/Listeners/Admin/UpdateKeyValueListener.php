<?php declare(strict_types=1);

namespace App\Listeners\Admin;

use App\Actions\Article\MakeModelByIdAction;
use App\Actions\Article\UpdateModelStatuses;
use App\Actions\Article\UpdateModelValuesByTranslateKeyAction;
use App\Events\Admin\UpdateKeyValueEvent;
use App\Listeners\ListenerAbstract;
use App\Models\TranslateKey;
use App\Tasks\GetByIdTask;

/**
 * Class UpdateArticleStatusesListener
 * @package App\Listeners
 */
class UpdateKeyValueListener extends ListenerAbstract
{
    /**
     * @param \App\Events\Admin\UpdateKeyValueEvent $event
     * @throws \Exception
     */
    public function handle(UpdateKeyValueEvent $event)
    {
        $keyId = $event->keyId;
        $translateKey = $this->manager->task(GetByIdTask::class)
            ->arguments(['id' => $keyId])
            ->model(TranslateKey::class)
            ->method('with', ['articles', 'langRu'])
            ->run();
        $articles = $translateKey->articles;

        foreach ($articles as $article) {
            $model = $this->manager->action(MakeModelByIdAction::class)
                ->arguments(['id' => $article->id])->run();
            $this->manager->action(UpdateModelValuesByTranslateKeyAction::class)
                ->arguments(['model' => $model, 'keyUpdate' => $translateKey->key, 'value' => $translateKey->langRu->value])
                ->run();
            $this->manager->action(UpdateModelStatuses::class)
                ->arguments(['model' => $model])->run();
        }
    }
}