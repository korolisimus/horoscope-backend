<?php declare(strict_types=1);

namespace App\Listeners\Admin;

use App\Actions\Article\MakeModelByIdAction;
use App\Events\Event;
use App\Listeners\ListenerAbstract;
use App\Models\Article;
use App\Models\GenerateDataInterface;
use App\Models\HasTypeInterface;


class CreateAdminModelListener extends ListenerAbstract
{
    /**
     * @param \App\Events\Admin\CreateAdminModelEvent $event
     * @return bool
     * @throws \Exception
     */
    public function handle(Event $event): bool
    {
        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => $event->id])->run();
        if ($model instanceof Article) {
            if ($model instanceof GenerateDataInterface) {
                $model->data = $model::generateData();
            }
            if ($model instanceof HasTypeInterface) {
                $model->type = $model::getType();
            }

            $model->save();
        }
        return true;
    }
}
