<?php declare(strict_types=1);

namespace App\Listeners\Admin;


use App\Actions\Article\MakeModelByIdAction;
use App\Actions\PublishedEntity\UpdateActiveEntityAction;
use App\Criterias\Core\EqualCriteria;
use App\Criterias\Core\LikeBeginCriteria;
use App\Events\Admin\DeleteAdminModelEvent;
use App\Listeners\ListenerAbstract;
use App\Models\PublishedEntity;
use App\Models\TranslateKey;
use App\Tasks\DeleteTask;

class DeleteAdminModelListener extends ListenerAbstract
{

    /**
     * @param \App\Events\Admin\DeleteAdminModelEvent $event
     * @return bool
     * @throws \Exception
     */
    public function handle(DeleteAdminModelEvent $event): bool
    {
        $id = $event->id;
        $this->manager->task(DeleteTask::class)
            ->model(TranslateKey::class)
            ->criteria(new LikeBeginCriteria('key', $id . "_"))
            ->run();
        $model = $this->manager->action(MakeModelByIdAction::class)
            ->arguments(['id' => $id])->run();
        $type = $model->getEntityType();
        $this->manager->task(DeleteTask::class)
            ->model(PublishedEntity::class)
            ->criteria(new EqualCriteria('entity_id', $id))
            ->criteria(new EqualCriteria('entity_type', $type))
            ->run();

        $this->manager->action(UpdateActiveEntityAction::class)->arguments([$type])->run();
        return true;
    }
}
