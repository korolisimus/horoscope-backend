<?php declare(strict_types=1);

namespace App\Listeners\Admin;

use App\Actions\Article\MakeModelByIdAction;
use App\Actions\Article\UpdateModelStatuses;
use App\Actions\Article\UpdateTranslateValuesAction;
use App\Actions\Translates\DeleteUnusedKeysAction;
use App\Actions\Translates\UpdateModelTranslateInfoAction;
use App\Events\Event;
use App\Listeners\ListenerAbstract;
use App\Models\TranslateModelInterface;

class UpdateAdminModelListener extends ListenerAbstract
{

    /**
     * @param \App\Events\Admin\UpdateAdminModelEvent $event
     * @return bool
     */
    public function handle(Event $event): bool
    {
        try {
            $model = $this->manager->action(MakeModelByIdAction::class)
                ->arguments(['id' => $event->id])->run();
            if ($model instanceof TranslateModelInterface) {
                $this->manager->action(UpdateModelTranslateInfoAction::class)
                    ->arguments(['model' => $model])->run();
                $this->manager->action(UpdateTranslateValuesAction::class)
                    ->arguments(['model' => $model])->run();
                $this->manager->action(DeleteUnusedKeysAction::class)
                    ->arguments(['model' => $model])->run();
                $this->manager->action(UpdateModelStatuses::class)
                    ->arguments(['model' => $model])->run();
            }
        } catch (\Throwable $e) {
            info('UpdateAdminModelListener error: ' . $e->getMessage());
            info($e->getFile());
            info($e->getLine());
            info($e->getTraceAsString());
        }

        return true;
    }
}
