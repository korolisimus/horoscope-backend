<?php declare(strict_types=1);

namespace App\Listeners;

use App\Manager;

class ListenerAbstract
{
    protected Manager $manager;

    public function __construct()
    {
        $this->manager = app('manager');
    }
}
