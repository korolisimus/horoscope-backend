<?php declare(strict_types=1);

namespace App;

use App\Mediators\Processor;
use Mockery\Exception;
use Prettus\Validator\LaravelValidator;

final class Manager
{
    private Processor $processor;

    private string $model;

    private string $repository;

    private array $arguments = [];

    private array $methods = [];

    private string $transformer;

    private array $extraMethods = [];

    private string $unit;

    private bool $call_task = false;

    private LaravelValidator $validator;

    private string $validatorAction;

    public function __construct(Processor $processor)
    {
        $this->processor = $processor;
    }

    public function task(string $task): Manager
    {
        $this->unit = $task;
        $this->call_task = true;
        return $this;
    }

    public function action(string $action): Manager
    {
        $this->unit = $action;
        $this->call_task = false;
        return $this;
    }

    public function model($model): Manager
    {
        $this->model = $model;
        return $this;
    }

    public function repository($repository): Manager
    {
        $this->repository = $repository;
        return $this;
    }

    public function arguments(array $arguments): Manager
    {
        $this->arguments = $arguments;
        return $this;
    }

    public function method(string $name, array $arguments): Manager
    {
        $this->methods[] = [$name => [$arguments]];
        return $this;
    }

    public function transformer($transformer): Manager
    {
        $this->transformer = $transformer;
        return $this;
    }

    public function validator(string $validator, string $action): Manager
    {
        $this->validator = app($validator);
        $this->validatorAction = $action;
        return $this;
    }

    public function criteria($criteria): Manager
    {
        if (!is_array($criteria)) {
            $criteria = [$criteria];
        }
        foreach ($criteria as $item) {
            $this->methods[] = ['addCriteria' => [$item]];
        }

        return $this;
    }

    public function extraMethod(string $name, array $arguments = []): Manager
    {
        $this->extraMethods[] = [$name => [$arguments]];
        return $this;
    }

    /**
     * @return mixed|null
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run()
    {
        if (!empty($this->validator)) {
            $this->validator->with($this->arguments)->passesOrFail($this->validatorAction);
        }
        list($event, $arguments, $methods, $postMethods) = $this->build();
        $this->reset();
        return $this->processor->call($event, $arguments, $methods, $postMethods);
    }

    private function build(): array
    {
        if (empty($this->unit)) {
            throw new Exception('укажите task или action');
        }

        if ($this->call_task && empty($this->model) && empty($this->repository)) {
            throw new Exception('укажите model или repository');
        }

        $collectMethods = [];
        if (!empty($this->repository)) {
            $collectMethods[] = ['repository' => [$this->repository]];
        } else if (!empty($this->model)) {
            $collectMethods[] = ['model' => [$this->model]];
        }

        $collectMethods = array_merge($collectMethods, $this->methods);

        $collectExtraMethods = [];

        if (!empty($this->transformer)) {
            $collectExtraMethods[] = ['transform' => [$this->transformer]];
        }

        $collectExtraMethods = array_merge($collectExtraMethods, $this->extraMethods);

        return [
            $this->unit,
            $this->arguments,
            $collectMethods,
            $collectExtraMethods
        ];
    }

    private function reset()
    {
        $this->extraMethods = [];
        $this->methods = [];
        $this->call_task = false;
        $this->transformer = '';
        $this->unit = '';
        $this->model = '';
        $this->repository = '';
        $this->arguments = [];
    }
}