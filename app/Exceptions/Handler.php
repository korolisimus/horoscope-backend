<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        //parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        info([
            //'request' => $request->all(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'class' => get_class($exception),
            'path' => $request->path()
            //'trace' => $exception->getTraceAsString()
        ]);

        if ($exception instanceof ValidatorException) {
            $errors = $exception->getMessageBag()->messages();
            $message = array_shift($errors);
            $message = array_shift($message);
            return response()->json([
                'error' => ['message' => $message, 'code' => 404],
            ], 404);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                'error' => ['message' => trans('RESOURCE_NOT_FOUND'), 'code' => 404],
            ], 404);
        }

        //$message = !env("APP_DEBUG") ? "Internal Server Error" : $exception->getMessage();
        $code = $exception->getCode();
        if (empty($code)) {
            $code = 500;
            $message = "Internal Server Error";
        } else {
            $message = $exception->getMessage();
        }
        return response(['error' => ['message' => $message, 'code' => $code]], $code);
    }

}
