<?php declare(strict_types=1);

namespace App\Constants;

/**
 * Class PublishStatusConstant
 * @package App\Constants
 */
class PublishStatusConstant
{
    public const UNPUBLISHED = 'unpublished';
    public const NEED_UPDATE = 'need_update';
    public const PUBLISHED = 'published';
}