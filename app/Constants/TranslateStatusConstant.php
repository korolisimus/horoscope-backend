<?php declare(strict_types=1);

namespace App\Constants;

class TranslateStatusConstant
{
    public const NEED_TRANSLATE = 'need_translate';
    public const NEED_UPDATE = 'need_update';
    public const PROCESS_TRANSLATE = 'process_translate';
    public const TRANSLATED = 'translated';
}