<?php declare(strict_types=1);

namespace App\Constants;

class ZodiacNamesConstant
{
    public const ZODIAC_NAMES = [
        'ram' => 'Овен',
        'bull' => 'Телец',
        'twins' => 'Близнецы',
        'cancer' => 'Рак',
        'lion' => 'Лев',
        'maiden' => 'Дева',
        'scales' => 'Весы',
        'scorpion' => 'Скорпион',
        'archer' => 'Стрелец',
        'capricorn' => 'Козерог',
        'aquarius' => 'Водолей',
        'fishes' => 'Рыбы'
    ];
}

