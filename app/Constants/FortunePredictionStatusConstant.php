<?php declare(strict_types=1);

namespace App\Constants;

class FortunePredictionStatusConstant
{
    public const SHOW = 'show';
    public const HIDE = 'hide';
    public const PROCESS = 'process';
}