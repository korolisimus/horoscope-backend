<?php declare(strict_types=1);

namespace App\Constants;

class ArticleTypesConstant
{
    public const FORTUNE_HAND = ['mind', 'heart', 'destiny', 'life'];
    public const FORTUNE_COFFEE = 'coffee';
    public const COMPARE = 'compare';
    public const HAIRCUT = 'haircut';
    public const DAILY = 'daily';
    public const ASTRO = 'astro';
    public const ZODIAC = 'zodiac';
}