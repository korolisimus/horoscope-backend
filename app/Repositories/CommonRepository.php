<?php declare(strict_types=1);

namespace App\Repositories;

use App\Traits\ModelTrait;

final class CommonRepository extends RepositoryAbstract
{
    use ModelTrait;
}