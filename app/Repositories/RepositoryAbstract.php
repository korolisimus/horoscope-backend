<?php declare(strict_types=1);

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;


abstract class RepositoryAbstract extends BaseRepository
{
    public function remove(): ?int
    {
        $this->applyCriteria();
        $this->applyScope();
        try {
            return $this->model->delete();
        } catch (\Exception $e) {}
        return null;
    }

    /**
     * @param array $params
     * @return bool|int
     */
    public function multipleUpdate(array $params = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        return $this->model->update($params);
    }


    public function insertOrUpdate(array $params = [])
    {
        $this->applyCriteria();
        $this->applyScope();
        return $this->model::insertOnDuplicateKey($params);
    }
}
