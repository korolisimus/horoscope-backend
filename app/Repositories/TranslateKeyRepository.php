<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\TranslateKey;
use App\Models\TranslateLang;
use App\Tasks\GetTask;

class TranslateKeyRepository extends RepositoryAbstract
{
    public function model(): string
    {
        return TranslateKey::class;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    public function insertLangs($data)
    {
        /** @var \App\Manager $manager */
        $manager = app('manager');
        $data['langs'] = $manager->task(GetTask::class)
                ->model(TranslateLang::class)
                ->run()->toArray() ?? [];
        return $data;
    }
}