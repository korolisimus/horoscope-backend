<?php

namespace App\Console;

use App\Console\Commands\PushUsersPredictionsCommand;
use App\Console\Commands\Sandbox;
use App\Console\Commands\UpdateDailyPublishedEntitiesCommand;
use App\Console\Commands\UpdateMonthPublishedEntitiesCommand;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Sandbox::class,
        UpdateMonthPublishedEntitiesCommand::class,
        UpdateDailyPublishedEntitiesCommand::class,
        PushUsersPredictionsCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command(UpdateDailyPublishedEntitiesCommand::class)->daily();
        $schedule->command(UpdateMonthPublishedEntitiesCommand::class)->monthly();
        $schedule->command(PushUsersPredictionsCommand::class)->everyMinute();
    }
}
