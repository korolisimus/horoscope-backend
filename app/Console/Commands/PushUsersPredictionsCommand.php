<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\User\PushUsersPredictionsAction;

class PushUsersPredictionsCommand extends CommandAbstract
{
    protected $signature = 'horoscope:push_users_predictions';

    protected $description = 'Push coffee and hand predictions to users';

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle()
    {
        $this->manager->action(PushUsersPredictionsAction::class)->run();
    }
}
