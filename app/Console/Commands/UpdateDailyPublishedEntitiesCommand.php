<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\PublishedEntity\UpdateDailyPublishedEntitiesAction;

class UpdateDailyPublishedEntitiesCommand extends CommandAbstract
{
    protected $signature = 'horoscope:update_daily_entities';

    protected $description = 'Update active status for daily entities';

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle()
    {
        $this->manager->action(UpdateDailyPublishedEntitiesAction::class)->run();
        $a = ['a' => 'b'];
    }
}
