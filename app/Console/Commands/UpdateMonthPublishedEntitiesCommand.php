<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Actions\PublishedEntity\UpdateMonthPublishedEntitiesAction;

class UpdateMonthPublishedEntitiesCommand extends CommandAbstract
{
    protected $signature = 'horoscope:update_month_entities';

    protected $description = 'Update active status for month entities';

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function handle()
    {
        $this->manager->action(UpdateMonthPublishedEntitiesAction::class)->run();
    }
}
