<?php declare(strict_types=1);

namespace App\Console\Commands;

use App\Manager;
use Illuminate\Console\Command;

abstract class CommandAbstract extends Command
{
    protected Manager $manager;

    public function __construct()
    {
        parent::__construct();
        $this->manager = app('manager');
    }
}