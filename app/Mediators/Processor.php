<?php declare(strict_types=1);

namespace App\Mediators;

class Processor
{
    public function call(
        $class,
        $runMethodArguments = [],
        $extraMethods = [],
        $extraPostMethods = []
    )
    {
        $class = $this->resolveClass($class);

        $this->callExtraMethods($class, $extraMethods);

        $data = $class->run(...array_values($runMethodArguments));

        return $this->callPostExtraMethods($class, $extraPostMethods, $data);
    }

    private function resolveClass($class): object
    {
        return app($class);
    }

    private function callExtraMethods($class, $extraMethods)
    {
        foreach ($extraMethods as $methodInfo) {
            if (is_array($methodInfo)) {
                $this->callWithArguments($class, $methodInfo);
            } else {
                $this->callWithoutArguments($class, $methodInfo);
            }
        }
    }

    private function callWithArguments($class, $methodInfo, $data = null, $post = false)
    {
        $method = key($methodInfo);
        $arguments = $methodInfo[$method];
        if (method_exists($class, $method)) {
            if (!empty($data)) {
                array_unshift($arguments, $data);
            } else {
                if ($post) {
                    array_unshift($arguments, null);
                }
            }
            $data = $class->$method(...array_values($arguments));

        }

        $repository = $class->getRepository();
        if (method_exists($repository, $method)) {
            if (!empty($data)) {
                array_unshift($arguments, $data);
            } else {
                if ($post) {
                    array_unshift($arguments, null);
                }
            }
            $data = $repository->$method(...array_values($arguments));
        }

        return $data;
    }

    private function callWithoutArguments($class, $methodInfo)
    {
        if (method_exists($class, $methodInfo)) {
            $class->$methodInfo();
        }
    }

    private function callPostExtraMethods($class, $extraMethods, $data)
    {
        foreach ($extraMethods as $methodInfo) {
            $data = $this->callWithArguments($class, $methodInfo, $data, true);
        }
        return $data;
    }
}
