<?php declare(strict_types=1);

namespace App\Tasks;

class UpdateTask extends TaskAbstract
{
    /**
     * @param int $id
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(int $id, array $params = [])
    {
        $item = $this->repository->skipPresenter()->find($id);
        $params = array_merge($item->toArray(), $params);
        return $this->repository->update($params, $id);
    }
}