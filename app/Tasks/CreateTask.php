<?php declare(strict_types=1);

namespace App\Tasks;

class CreateTask extends TaskAbstract
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [])
    {
        return $this->repository->create($params);
    }
}