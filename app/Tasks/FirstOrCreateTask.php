<?php declare(strict_types=1);

namespace App\Tasks;

/**
 * Class FirstOrCreateTask
 * @package App\Tasks
 */
class FirstOrCreateTask extends TaskAbstract
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     */
    public function run(array $params = [])
    {
        return $this->repository->firstOrCreate($params);
    }
}
