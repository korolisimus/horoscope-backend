<?php declare(strict_types=1);

namespace App\Tasks;

class PaginateTask extends TaskAbstract
{
    public function run(int $limit = 10)
    {
        return $this->repository->paginate($limit);
    }
}
