<?php declare(strict_types=1);

namespace App\Tasks;

use App\Traits\RepositoryTrait;

abstract class TaskAbstract
{
    use RepositoryTrait;

    public function setup()
    {
    }
}