<?php declare(strict_types=1);

namespace App\Tasks;

class DeleteTask extends TaskAbstract
{
    public function run(): bool
    {
        return (bool)$this->repository->remove();
    }
}
