<?php declare(strict_types=1);

namespace App\Tasks;

class InsertOrUpdateTask extends TaskAbstract
{
    public function run(array $params = [])
    {
        return $this->repository->insertOrUpdate($params);
    }
}