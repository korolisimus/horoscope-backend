<?php declare(strict_types=1);

namespace App\Tasks;

class DeleteByIdTask extends TaskAbstract
{
    public function run(int $id): bool
    {
        return (bool)$this->repository->delete($id);
    }
}
