<?php declare(strict_types=1);

namespace App\Tasks;

class GetFirstTask extends TaskAbstract
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     */
    public function run()
    {
        $data = $this->repository->first();
        if (empty($data)) {
            $data = [];
        }
        return $data;
    }
}