<?php declare(strict_types=1);

namespace App\Tasks;

class GetByIdTask extends TaskAbstract
{
    public function run(int $id)
    {
        return $this->repository->find($id);
    }
}