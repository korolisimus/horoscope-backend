<?php declare(strict_types=1);

namespace App\Tasks;

class UpdateOrCreateTask extends TaskAbstract
{
    /**
     * @param array $params
     * @param array $values
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function run(array $params = [], array $values = [])
    {
        return $this->repository->updateOrCreate($params, $values);
    }
}