<?php declare(strict_types=1);

namespace App\Tasks;

class MultipleUpdateTask extends TaskAbstract
{
    public function run(array $params = [])
    {
        return $this->repository->multipleUpdate($params);
    }

    public function setup()
    {
        parent::setup();
        $this->required_methods[] = 'addCriteria';
    }
}