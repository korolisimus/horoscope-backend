<?php declare(strict_types=1);

namespace App\Tasks;

class GetByFieldTask extends TaskAbstract
{
    public function run(string $field, $value)
    {
        return $this->repository->where($field, '=', $value)->first();
    }
}