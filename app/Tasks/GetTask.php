<?php declare(strict_types=1);

namespace App\Tasks;

class GetTask extends TaskAbstract
{
    public function run()
    {
        return $this->repository->get();
    }
}